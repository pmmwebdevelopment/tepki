import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Form,
    FormGroup,
    Label,
    CustomInput
    // CustomInput,
    // Button
} from 'reactstrap';
import VerbElementCard from './VerbElementCard'
import VerbElementAlert from './VerbElementAlert'
import axios from 'axios';
import {
    setChosenVerbTurkish,
    setChosenVerbStem,
    setChosenVerbEnglish,
    setChosenVerbEnglishGerund,
    setChosenVerbEnglishThirdPersonSingularPresent,
    setChosenVerbEnglishThirdPersonPluralPresent,
    setChosenVerbEnglishPast,
    setChosenVerbEnglishThirdPersonPluralPast,
    setChosenVerbEnglishPastParticiple,
    setChosenVerbTurkishPassiveAvailable,
    setChosenVerbVoice,
    setChosenVerbExtraCausative,
    setChosenVerbInability,
    setChosenVerbNegative,
    setChosenVerbFiniteness,
    setChosenVerbTAM2,
    setChosenVerbTAM3,
    setChosenVerbTAM4,
    setChosenVerbPerson,
    setChosenVerbModality,
    setChosenVerbSubordinatingSuffix,
    setChosenVerbPossessionMarker,
    setChosenVerbCaseMarker,
    setChosenVerbVoiceParticle,
    setChosenVerbExtraCausativeParticle,
    setChosenVerbInabilityParticle,
    setChosenVerbNegativeParticle,
    setChosenVerbTAM2Particle,
    setChosenVerbTAM3Particle,
    setChosenVerbTAM4Particle,
    setChosenVerbPersonParticle,
    setChosenVerbModalityParticle,
    setChosenVerbSubordinatingSuffixParticle,
    setChosenVerbPossessionMarkerParticle,
    setChosenVerbCaseMarkerParticle,
} from '../../../store/actions';
import { connect } from 'react-redux';
import classes from './VerbEngine.module.css';

class VerbEngine extends Component {

    constructor(props) {
        super(props);

        this.verbDropdownToggle = this.verbDropdownToggle.bind(this);
        this.handleVoiceChoice = this.handleVoiceChoice.bind(this);
        this.handleExtraCausativeChoice = this.handleExtraCausativeChoice.bind(this);
        this.handleInabilityChoice = this.handleInabilityChoice.bind(this);
        this.handleNegativeChoice = this.handleNegativeChoice.bind(this);
        this.handleFinitenessChoice = this.handleFinitenessChoice.bind(this);
        this.handleTAM2Choice = this.handleTAM2Choice.bind(this);
        this.handleTAM3Choice = this.handleTAM3Choice.bind(this);
        this.handleTAM4Choice = this.handleTAM4Choice.bind(this);
        this.handlePersonChoice = this.handlePersonChoice.bind(this);
        this.handleModalityChoice = this.handleModalityChoice.bind(this);
        this.handleSubordinatingSuffixChoice = this.handleSubordinatingSuffixChoice.bind(this);
        this.handlePossessionMarkerChoice = this.handlePossessionMarkerChoice.bind(this);
        this.handleCaseMarkerChoice = this.handleCaseMarkerChoice.bind(this);
        this.translate = this.translate.bind(this);
    
        this.state = {
            loading: false,
            verbList: [],
            verbDropdownOpen: false,
            verbDropdownItems: [],
            translation: ''
        }
    
    }

    componentDidMount() {
        this.getVerbList();
    }

    getVerbList() {
        this.setState({loading: true});
        axios.get('https://tepki-pmm2.firebaseio.com/verbs.json?orderBy="en"')
        .then(response => {
            this.setState({
                    loading: false,
                    verbList: response.data
            }, () => this.setVerbDropdownItems());
        }).catch(error => {
            console.log('There has been an error');
            this.setState({loading: false});
        });
    }

    setVerbDropdownItems() {
        let _verbDropdownItems = this.state.verbList.map((verb, index) => 
            <DropdownItem onClick={() => this.handleVerbChoice(verb)} key={index}>
                <b>{verb.name}</b> - {verb.en}
            </DropdownItem>
        );
        this.setState({ verbDropdownItems: _verbDropdownItems });
    }

    verbDropdownToggle() {
        this.setState({ verbDropdownOpen: !this.state.verbDropdownOpen });
    }

    handleVerbChoice(_verb) {
        let en3pluralpres = !_verb.en3ppres ? _verb.en.replace('to ', '') : _verb.en3ppres;
        let en3pluralpast = !_verb.en3pastp ? _verb.en3past : _verb.en3pastp;
        let enPastParticiple = !_verb.enparticiple ? _verb.en3past : _verb.enparticiple;
        this.props.setChosenVerbTurkish(_verb.name);
        this.props.setChosenVerbStem(_verb.stem);
        this.props.setChosenVerbEnglish(_verb.en);
        this.props.setChosenVerbEnglishGerund(_verb.engerund);
        this.props.setChosenVerbEnglishThirdPersonSingularPresent(_verb.en3pspres);
        this.props.setChosenVerbEnglishThirdPersonPluralPresent(en3pluralpres);
        this.props.setChosenVerbEnglishPast(_verb.en3past);
        this.props.setChosenVerbEnglishThirdPersonPluralPast(en3pluralpast);
        this.props.setChosenVerbEnglishPastParticiple(enPastParticiple);
        this.props.setChosenVerbTurkishPassiveAvailable(_verb.passive);
        this.props.setChosenVerbVoice('');
        this.props.setChosenVerbExtraCausative(false);
        this.props.setChosenVerbInability(false);
        this.props.setChosenVerbNegative(false);
        this.props.setChosenVerbFiniteness('');
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbVoiceParticle('');
        this.props.setChosenVerbExtraCausativeParticle('');
        this.props.setChosenVerbInabilityParticle('');
        this.props.setChosenVerbNegativeParticle('');
        this.verbDropdownToggle();
    }

    checkForVowelEnding(string) {
        let vowels = ['a', 'e', 'ı', 'o', 'ö', 'u', 'ü'];
        for (let v = 0; v < vowels.length; v += 1) {
            if(string[string.length - 1] === vowels[v]) { return true; } else { continue; }
        }
        return false;
    }

    getLastVowelInString(string) {
        let vowels = ['a', 'e', 'ı', 'o', 'ö', 'u', 'ü'];
        for (let letter = string.length - 1; letter >= 0; letter -= 1) {
            if(vowels.includes(string[letter])) { return string[letter]; } else { continue; } 
        }
        return;
    }

    checkForVoicelessEndingConsonant(string) {
        let voiceless = ['ç', 'f', 'k', 'p', 's', 'ş', 't'];
        return voiceless.includes(string[string.length - 1]);
    }

    setCausativeParticle() {
        let headlessIRCausativeStems = ['art', 'bat', 'kaç', 'şaş', 'taş'];
        let dottedIRCausativeStems = ['bit', 'geç', 'iç', 'piş', 'şiş'];
        let uRCausativeStems = ['doğ', 'doy', 'uç'];
        let uUmlautRCausativeStems = ['düş', 'göç'];
        let headlessITCausativeStems = ['ak', 'sark'];
        let uUmlautTCausativeStems = ['ürk'];
        let aRCausativeStems = ['çık', 'kop', 'on'];
        if(headlessIRCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ır')}
        else if(dottedIRCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ir')}
        else if(uRCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ur')}
        else if(uUmlautRCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ür')}
        else if(headlessITCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ıt')}
        else if(uUmlautTCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('üt')}
        else if(aRCausativeStems.includes(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ar')}
        else if(this.props.chosenVerbStem === 'git') {
            this.props.setChosenVerbStem('gid');
            this.props.setChosenVerbVoiceParticle('er');
        }
        else if(this.props.chosenVerbStem === 'gör') {
            this.props.setChosenVerbStem('göst');
            this.props.setChosenVerbVoiceParticle('er');
        }
        else if((this.props.chosenVerbStem[this.props.chosenVerbStem.length - 1] === 'l') ||
                (this.props.chosenVerbStem[this.props.chosenVerbStem.length - 1] === 'r') ||
                this.checkForVowelEnding(this.props.chosenVerbStem))  {
            this.props.setChosenVerbVoiceParticle('t');
        }
        else {
            switch(this.getLastVowelInString(this.props.chosenVerbStem)) {
                case 'a':
                case 'ı':
                    if(this.checkForVoicelessEndingConsonant(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('tır'); }
                        else { this.props.setChosenVerbVoiceParticle('dır'); }
                    break;
                case 'o':
                case 'u':
                    if(this.checkForVoicelessEndingConsonant(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('tur'); }
                        else { this.props.setChosenVerbVoiceParticle('dur'); }
                    break;
                case 'e':
                case 'i':
                    if(this.checkForVoicelessEndingConsonant(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('tir'); }
                        else { this.props.setChosenVerbVoiceParticle('dir'); }
                    break;
                case 'ö':
                case 'ü':
                    if(this.checkForVoicelessEndingConsonant(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('tür'); }
                        else { this.props.setChosenVerbVoiceParticle('dür'); }
                    break;
                default:
                    this.props.setChosenVerbVoiceParticle('');
            }
        } 
    }

    setExtraCausativeParticle() {
        let stringBeforeExtraCausative = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle;
        switch(this.getLastVowelInString(stringBeforeExtraCausative)) {
            case 'a':
            case 'ı':
                if(this.checkForVoicelessEndingConsonant(stringBeforeExtraCausative)) { this.props.setChosenVerbExtraCausativeParticle('tır'); }
                    else { this.props.setChosenVerbExtraCausativeParticle('dır'); }
                break;
            case 'o':
            case 'u':
                if(this.checkForVoicelessEndingConsonant(stringBeforeExtraCausative)) { this.props.setChosenVerbExtraCausativeParticle('tur'); }
                    else { this.props.setChosenVerbExtraCausativeParticle('dur'); }
                break;
            case 'e':
            case 'i':
                if(this.checkForVoicelessEndingConsonant(stringBeforeExtraCausative)) { this.props.setChosenVerbExtraCausativeParticle('tir'); }
                    else { this.props.setChosenVerbExtraCausativeParticle('dir'); }
                break;
            case 'ö':
            case 'ü':
                if(this.checkForVoicelessEndingConsonant(stringBeforeExtraCausative)) { this.props.setChosenVerbExtraCausativeParticle('tür'); }
                    else { this.props.setChosenVerbExtraCausativeParticle('dür'); }
                break;
            default:
                this.props.setChosenVerbExtraCausativeParticle('');
        }      
    }

    setPassiveParticle() {
        if(this.checkForVowelEnding(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('n'); }
        else {
            let passiveParticle = ''; 
            switch(this.getLastVowelInString(this.props.chosenVerbStem)) {
                case 'a':
                case 'ı':
                    passiveParticle = 'ı';
                    break;
                case 'o':
                case 'u':
                    passiveParticle = 'u';
                    break;
                case 'e':
                case 'i':
                    passiveParticle = 'i';
                    break;
                case 'ö':
                case 'ü':
                    passiveParticle = 'ü';
                    break;
                default:
                    passiveParticle = '';
            }
            if(this.props.chosenVerbStem[this.props.chosenVerbStem.length - 1] === 'l'){
                passiveParticle += 'n';
            } else {
                passiveParticle += 'l';
            }
            this.props.setChosenVerbVoiceParticle(passiveParticle);
        }
    }

    setReciprocalParticle() {
        if(this.checkForVowelEnding(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('ş'); }
        else {
            let reciprocalParticle = '';
            switch(this.getLastVowelInString(this.props.chosenVerbStem)) {
                case 'a':
                case 'ı':
                    reciprocalParticle = 'ış';
                    break;
                case 'o':
                case 'u':
                    reciprocalParticle = 'uş';
                    break;
                case 'e':
                case 'i':
                    reciprocalParticle = 'iş';
                    break;
                case 'ö':
                case 'ü':
                    reciprocalParticle = 'üş';
                    break;
                default:
                    reciprocalParticle = '';
            }
            this.props.setChosenVerbVoiceParticle(reciprocalParticle);
        }
    }

    setReflexiveParticle() {
        if(this.checkForVowelEnding(this.props.chosenVerbStem)) { this.props.setChosenVerbVoiceParticle('n'); }
        else {
            let reflexiveParticle = '';
            switch(this.getLastVowelInString(this.props.chosenVerbStem)) {
                case 'a':
                case 'ı':
                    reflexiveParticle = 'ın';
                    break;
                case 'o':
                case 'u':
                    reflexiveParticle = 'un';
                    break;
                case 'e':
                case 'i':
                    reflexiveParticle = 'in';
                    break;
                case 'ö':
                case 'ü':
                    reflexiveParticle = 'ün';
                    break;
                default:
                    reflexiveParticle = '';

            }
            this.props.setChosenVerbVoiceParticle(reflexiveParticle);
        }
    }

    setInabilityParticle() {
        let stringBeforeInability = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbExtraCausativeParticle;
        let inabilityParticle = '';
        if(this.checkForVowelEnding(stringBeforeInability)) { inabilityParticle = 'y'; }
        switch(this.getLastVowelInString(stringBeforeInability)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                inabilityParticle += 'a';
                this.props.setChosenVerbNegativeParticle('ma');
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                inabilityParticle = 'e';
                this.props.setChosenVerbNegativeParticle('me');
                break;
            default:
                this.props.setChosenVerbNegativeParticle('');

        }
        this.props.setChosenVerbInabilityParticle(inabilityParticle);
    }

    setNegativeParticle() {
        let stringBeforeNegative =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + 
            this.props.chosenVerbExtraCausativeParticle + this.props.chosenVerbInabilityParticle;
        switch(this.getLastVowelInString(stringBeforeNegative)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                this.props.setChosenVerbNegativeParticle('ma');
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                this.props.setChosenVerbNegativeParticle('me');
                break;
            default:
                this.props.setChosenVerbNegativeParticle('');
        }
    }

    handleVoiceChoice(changeEvent) {
        this.props.setChosenVerbVoice(changeEvent.target.value);
        switch(changeEvent.target.value) {
            case "Active":
            case "Imperative":
            default:
                this.props.setChosenVerbVoiceParticle('');
                break;
            case "Causative":
                this.setCausativeParticle();
                break;
            case "Passive":
                this.setPassiveParticle();
                break;
            case "Reciprocal":
                this.setReciprocalParticle();
                break;
            case "Reflexive":
                this.setReflexiveParticle();
                break;
        }
        this.props.setChosenVerbExtraCausative(false);
        this.props.setChosenVerbInability(false);
        this.props.setChosenVerbNegative(false);
        this.props.setChosenVerbFiniteness('');
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbTAM2Particle('');
        this.props.setChosenVerbTAM3Particle('');
        this.props.setChosenVerbTAM4Particle('');
        this.props.setChosenVerbPersonParticle('');
        this.props.setChosenVerbModalityParticle('');
        this.props.setChosenVerbSubordinatingSuffixParticle('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');  
    }
    
    handleExtraCausativeChoice(changeEvent) {
        if(changeEvent.target.checked) {
            this.props.setChosenVerbExtraCausative(true);
            this.setExtraCausativeParticle();
        } else {
            this.props.setChosenVerbExtraCausative(false);
            this.props.setChosenVerbExtraCausativeParticle('');
        }
        this.props.setChosenVerbInability(false);
        this.props.setChosenVerbNegative(false);
        this.props.setChosenVerbFiniteness('');
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbTAM2Particle('');
        this.props.setChosenVerbTAM3Particle('');
        this.props.setChosenVerbTAM4Particle('');
        this.props.setChosenVerbPersonParticle('');
        this.props.setChosenVerbModalityParticle('');
        this.props.setChosenVerbSubordinatingSuffixParticle('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }

    handleInabilityChoice(changeEvent) {
        if(changeEvent.target.checked) {
            this.props.setChosenVerbInability(true);
            this.props.setChosenVerbNegative(true);
            this.setInabilityParticle();
            this.setNegativeParticle();
            this.props.setChosenVerbTAM2('');
            this.props.setChosenVerbTAM2Particle('');
        
        } else {
            this.props.setChosenVerbInability(false);
            this.props.setChosenVerbNegative(false);
            this.props.setChosenVerbInabilityParticle('');
            this.props.setChosenVerbNegativeParticle('');
        }
        this.props.setChosenVerbFiniteness('');
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbTAM2Particle('');
        this.props.setChosenVerbTAM3Particle('');
        this.props.setChosenVerbTAM4Particle('');
        this.props.setChosenVerbPersonParticle('');
        this.props.setChosenVerbModalityParticle('');
        this.props.setChosenVerbSubordinatingSuffixParticle('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }

    handleNegativeChoice(changeEvent) {
        if(changeEvent.target.value === 'Yes') {
            this.props.setChosenVerbNegative(true);
            this.setNegativeParticle();
            this.props.setChosenVerbTAM2('');
            this.props.setChosenVerbTAM2Particle('');
        } else {
            this.props.setChosenVerbNegative(false);
            this.props.setChosenVerbNegativeParticle('');
        }
        this.props.setChosenVerbFiniteness('');
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbTAM2Particle('');
        this.props.setChosenVerbTAM3Particle('');
        this.props.setChosenVerbTAM4Particle('');
        this.props.setChosenVerbPersonParticle('');
        this.props.setChosenVerbModalityParticle('');
        this.props.setChosenVerbSubordinatingSuffixParticle('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }
    
    handleFinitenessChoice(changeEvent) {
        this.props.setChosenVerbFiniteness(changeEvent.target.value);
        this.props.setChosenVerbTAM2('');
        this.props.setChosenVerbTAM3('');
        this.props.setChosenVerbTAM4('');
        this.props.setChosenVerbPerson('');
        this.props.setChosenVerbModality(false);
        this.props.setChosenVerbSubordinatingSuffix('');
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbTAM2Particle('');
        this.props.setChosenVerbTAM3Particle('');
        this.props.setChosenVerbTAM4Particle('');
        this.props.setChosenVerbPersonParticle('');
        this.props.setChosenVerbModalityParticle('');
        this.props.setChosenVerbSubordinatingSuffixParticle('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }
    

    setAbilityParticle() {
        let stringBeforeAbility = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle;
        let abilityParticle = '';
        if(this.checkForVowelEnding(stringBeforeAbility)) { abilityParticle = 'y'; }
        switch(this.getLastVowelInString(stringBeforeAbility)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                abilityParticle += 'abıl';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                abilityParticle += 'ebil';
                break;
            default:
                this.props.setChosenVerbTAM2Particle('');
            }
        this.props.setChosenVerbTAM2Particle(abilityParticle);
    }

    setHappenToParticle() {
        let stringBeforeHappenTo = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle;
        let happenToParticle = '';
        if(this.checkForVowelEnding(stringBeforeHappenTo)) { happenToParticle = 'y'; }
        switch(this.getLastVowelInString(stringBeforeHappenTo)) {
            case 'a':
            case 'ı':
                happenToParticle += 'ıver';
                break;
            case 'o':
            case 'u':
                happenToParticle += 'uver';
                break;
            case 'e':
            case 'i':
                happenToParticle += 'iver';
                break;
            case 'ö':
            case 'ü':
                happenToParticle += 'üver';
                break;
            default:
                this.props.setChosenVerbTAM2Particle('');
        }
        this.props.setChosenVerbTAM2Particle(happenToParticle);
    }

    setKeepOnParticle() {
        let stringBeforeKeepOn = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle;
        let keepOnParticle = '';
        if(this.checkForVowelEnding(stringBeforeKeepOn)) { keepOnParticle = 'y'; }
        switch(this.getLastVowelInString(stringBeforeKeepOn)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                keepOnParticle += 'agel';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                keepOnParticle += 'egel';
                break;
            default:
                this.props.setChosenVerbTAM2Particle('');
        }
        this.props.setChosenVerbTAM2Particle(keepOnParticle);
    }

    setAlmostParticle() {
        let stringBeforeAlmost = this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle;
        let almostParticle = '';
        if(this.checkForVowelEnding(stringBeforeAlmost)) { almostParticle = 'y'; }
        switch(this.getLastVowelInString(stringBeforeAlmost)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                almostParticle += 'ayaz';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                almostParticle += 'eyaz';
                break;
            default:
                this.props.setChosenVerbTAM2Particle('');
        }
        this.props.setChosenVerbTAM2Particle(almostParticle);
    }

    setTAM2Particle(_value) {
        switch(_value) {
            case "Ability":
                this.setAbilityParticle();
                break;
            case "Happen to...":
                this.setHappenToParticle();
                break;
            case "Keep on...":
                this.setKeepOnParticle();
                break;
            case "Almost":
                this.setAlmostParticle();
                break;
            default:
                this.props.setChosenVerbTAM2Particle('')
        }
    }
    
    handleTAM2Choice(changeEvent) {
        this.props.setChosenVerbTAM2(changeEvent.target.value);
        this.setTAM2Particle(changeEvent.target.value);
        setTimeout(() => {
            this.props.setChosenVerbTAM3('');
            this.props.setChosenVerbTAM4('');
            this.props.setChosenVerbPerson('');
            this.props.setChosenVerbModality(false);
            this.props.setChosenVerbSubordinatingSuffix('');
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
            this.props.setChosenVerbTAM3Particle('');
            this.props.setChosenVerbTAM4Particle('');
            this.props.setChosenVerbPersonParticle('');
            this.props.setChosenVerbModalityParticle('');
            this.props.setChosenVerbSubordinatingSuffixParticle('');
            this.props.setChosenVerbPossessionMarkerParticle('');
            this.props.setChosenVerbCaseMarkerParticle('');
        });
    }

    setPerfectiveParticle() {
        let stringBeforePerfective =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let perfectiveParticle = '';
        if(this.checkForVoicelessEndingConsonant(stringBeforePerfective)) {
            perfectiveParticle += 't';
        } else {
            perfectiveParticle += 'd';
        }
        switch(this.getLastVowelInString(stringBeforePerfective)) {
            case 'a':
            case 'ı':
                perfectiveParticle += 'ı';
                break;
            case 'o':
            case 'u':
                perfectiveParticle += 'u';
                break;
            case 'e':
            case 'i':
                perfectiveParticle += 'i';
                break;
            case 'ö':
            case 'ü':
                perfectiveParticle += 'ü';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(perfectiveParticle);
    }

    setApparentlyParticle() {
        let stringBeforeApparently =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let apparentlyParticle = '';
        switch(this.getLastVowelInString(stringBeforeApparently)) {
            case 'a':
            case 'ı':
                apparentlyParticle = 'mış';
                break;
            case 'o':
            case 'u':
                apparentlyParticle += 'muş';
                break;
            case 'e':
            case 'i':
                apparentlyParticle += 'miş';
                break;
            case 'ö':
            case 'ü':
                apparentlyParticle += 'müş';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(apparentlyParticle);
    }
    
    setIfParticle() {
        let stringBeforeIf =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let ifParticle = '';
        switch(this.getLastVowelInString(stringBeforeIf)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                ifParticle += 'sa';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                ifParticle += 'se';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(ifParticle);
    }
    
    setAoristParticle() {
        let monosyllableStems = ['al', 'bil', 'bul', 'dur', 'gel', 'gör', 'kal', 'ol', 'öl', 'san', 'ver', 'var', 'vur'];
        let tToDStems = ['git', 'et', 'tat', 'dit', 'güt'];
        let stringBeforeAorist =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let aoristParticle = '';
        if(this.props.chosenVerbNegativeParticle !== '') { aoristParticle = 'z'}
        else {
            if(monosyllableStems.includes(this.props.chosenVerbStem)) {
                switch(this.getLastVowelInString(this.props.chosenVerbStem)) {
                    case 'a':
                    case 'ı':
                        aoristParticle = 'ır';
                        break;
                    case 'o':
                    case 'u':
                        aoristParticle += 'ur';
                        break;
                    case 'e':
                    case 'i':
                        aoristParticle += 'ir';
                        break;
                    case 'ö':
                    case 'ü':
                        aoristParticle += 'ür';
                        break;
                }
            } else {
                if(tToDStems.includes(this.props.chosenVerbStem) && !this.props.chosenVerbInability && !this.props.chosenVerbNegative && this.props.chosenVerbTAM2 === '') {
                    this.props.chosenVerbStem.replace(this.props.chosenVerbStem[this.props.chosenVerbStem.length - 1], 'd')
                }
                switch(this.getLastVowelInString(stringBeforeAorist)) {
                    case 'a':
                    case 'ı':
                    case 'o':
                    case 'u':
                        aoristParticle += 'ar';
                        break;
                    case 'e':
                    case 'i':
                    case 'ö':
                    case 'ü':
                        aoristParticle += 'er';
                        break;
                    default:
                        this.props.setChosenVerbTAM3Particle('');
                }
            }
        }
        this.props.setChosenVerbTAM3Particle(aoristParticle);
    }
    
    setFutureParticle() {
        let stringBeforeFuture =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let futureParticle = '';
        if(this.props.chosenVerbNegative) {
            if(this.props.chosenVerbNegativeParticle === 'ma') {
                this.props.setChosenVerbNegative('mı');
            } else {
                this.props.setChosenVerbNegative('mi');
            }
        }
        if(this.checkForVowelEnding(stringBeforeFuture)) { futureParticle += 'y'}
        switch(this.getLastVowelInString(stringBeforeFuture)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                futureParticle += 'acak';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                futureParticle += 'ecek';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        if((this.props.chosenVerbTAM4 === '') && (this.props.chosenVerbPerson !== '')) {
            if(['a', 'e', 'ı', 'i', 'o', 'ö', 'u', 'ü'].includes(this.props.chosenVerbPersonParticle[0])) {
                futureParticle = futureParticle.replace('k', 'ğ');
            } 
        }
        setTimeout(() => {
            this.props.setChosenVerbTAM3Particle(futureParticle);
        });
    }
    
    setImperfectiveParticle() {
        let stringBeforeImperfective = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let imperfectiveParticle = '';
        if(this.props.chosenVerbNegative) {
            if(this.props.chosenVerbNegativeParticle === 'ma') {
                this.props.setChosenVerbNegative('mı');
            } else {
                switch(this.props.chosenVerbNegativeParticle) {
                    case 'e':
                    case 'i':
                        this.props.setChosenVerbNegative('mi');
                        break;
                    case 'ö':
                    case 'ü':
                        this.props.setChosenVerbNegative('mü');
                        break;
                }
            }
        }
        if(this.checkForVowelEnding(stringBeforeImperfective)) { imperfectiveParticle = 'yor'}
        switch(this.getLastVowelInString(stringBeforeImperfective)) {
            case 'a':
            case 'ı':
                imperfectiveParticle = 'ıyor';
                break;
            case 'o':
            case 'u':
                imperfectiveParticle = 'uyor';
                break;
            case 'e':
            case 'i':
                imperfectiveParticle = 'iyor';
                break;
            case 'ö':
            case 'ü':
                imperfectiveParticle += 'üyor';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(imperfectiveParticle);
    }
    
    setMustParticle() {
        let stringBeforeMust =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let mustParticle = '';
        switch(this.getLastVowelInString(stringBeforeMust)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                mustParticle += 'malı';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                mustParticle += 'meli';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(mustParticle);
    }
    
    setLetsParticle() {
        let stringBeforeLets =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle;
        let letsParticle = '';
        if(this.checkForVowelEnding(stringBeforeLets)) { letsParticle += 'y'}
        switch(this.getLastVowelInString(stringBeforeLets)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                letsParticle += 'a';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                letsParticle += 'e';
                break;
            default:
                this.props.setChosenVerbTAM3Particle('');
        }
        this.props.setChosenVerbTAM3Particle(letsParticle);
    }

    setTAM3Particle(_value) {
        switch(_value) {
            case "Perfective":
                this.setPerfectiveParticle();
                break;
            case "Apparently...":
                this.setApparentlyParticle();
                break;
            case "If...":
                this.setIfParticle();
                break;
            case "Aorist":
                this.setAoristParticle();
                break;
            case "Future":
                this.setFutureParticle();
                break;
            case "Imperfective":
                this.setImperfectiveParticle();
                break;
            case "Must...":
                this.setMustParticle();
                break;
            case "Let's...":
                this.setLetsParticle();
                break;
            default:
                this.props.setChosenVerbTAM3Particle('')
        }
        setTimeout(() => {
            this.setTAM2Particle(this.props.chosenVerbTAM2);
        });
    }

    handleTAM3Choice(changeEvent) {
        this.props.setChosenVerbTAM3(changeEvent.target.value);
        this.setTAM3Particle(changeEvent.target.value);
        setTimeout(() => {
            this.props.setChosenVerbTAM4('');
            this.props.setChosenVerbPerson('');
            this.props.setChosenVerbModality(false);
            this.props.setChosenVerbSubordinatingSuffix('');
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
            this.props.setChosenVerbTAM4Particle('');
            this.props.setChosenVerbPersonParticle('');
            this.props.setChosenVerbModalityParticle('');
            this.props.setChosenVerbSubordinatingSuffixParticle('');
            this.props.setChosenVerbPossessionMarkerParticle('');
            this.props.setChosenVerbCaseMarkerParticle('');
        });
    }

    setPastCopulaParticle() {
        let stringBeforePastCopula =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle;
        let pastCopulaParticle = '';
        if(this.checkForVowelEnding(stringBeforePastCopula)) {
            pastCopulaParticle += 'yd'
        } else if(this.checkForVoicelessEndingConsonant(stringBeforePastCopula)) {
            pastCopulaParticle += 't';
        } else {
            pastCopulaParticle += 'd';
        }
        switch(this.getLastVowelInString(stringBeforePastCopula)) {
            case 'a':
            case 'ı':
                pastCopulaParticle += 'ı';
                break;
            case 'o':
            case 'u':
                pastCopulaParticle += 'u';
                break;
            case 'e':
            case 'i':
                pastCopulaParticle += 'i';
                break;
            case 'ö':
            case 'ü':
                pastCopulaParticle += 'ü';
                break;
            default:
                this.props.setChosenVerbTAM4Particle('');
        }
        this.props.setChosenVerbTAM4Particle(pastCopulaParticle);
    }
    
    setEvidentialCopulaParticle() {
        let stringBeforeEvidentialCopula =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle;
        let evidentialCopulaParticle = '';
        if(this.checkForVowelEnding(stringBeforeEvidentialCopula)) { evidentialCopulaParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeEvidentialCopula)) {
            case 'a':
            case 'ı':
                evidentialCopulaParticle += 'mış';
                break;
            case 'o':
            case 'u':
                evidentialCopulaParticle += 'muş';
                break;
            case 'e':
            case 'i':
                evidentialCopulaParticle += 'miş';
                break;
            case 'ö':
            case 'ü':
                evidentialCopulaParticle += 'müş';
                break;
            default:
                this.props.setChosenVerbTAM4Particle('');
        }
        this.props.setChosenVerbTAM4Particle(evidentialCopulaParticle);
    }
    
    setConditionalCopulaParticle() {
        let stringBeforeConditionalCopula =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle;
        let conditionCopulaParticle = '';
        if(this.checkForVowelEnding(stringBeforeConditionalCopula)) { conditionCopulaParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeConditionalCopula)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                conditionCopulaParticle += 'sa';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                conditionCopulaParticle += 'se';
                break;
            default:
                this.props.setChosenVerbTAM4Particle('');
        }
        this.props.setChosenVerbTAM4Particle(conditionCopulaParticle);
    }

    setTAM4Particle(_value) {
        switch(_value) {
            case "Past_Copula":
                this.setPastCopulaParticle();
                break;
            case "Evidential_Copula":
                this.setEvidentialCopulaParticle();
                break;
            case "Conditional_Copula":
                this.setConditionalCopulaParticle();
                break;
            default:
                this.props.setChosenVerbTAM4Particle('');
        }
        setTimeout(() => {
            this.setTAM2Particle(this.props.chosenVerbTAM2);
            this.setTAM3Particle(this.props.chosenVerbTAM3);
        });
    }
    
    handleTAM4Choice(changeEvent) {
        this.props.setChosenVerbTAM4(changeEvent.target.value);
        this.setTAM4Particle(changeEvent.target.value);
        setTimeout(() => {
            this.props.setChosenVerbPerson('');
            this.props.setChosenVerbModality(false);
            this.props.setChosenVerbSubordinatingSuffix('');
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
            this.props.setChosenVerbPersonParticle('');
            this.props.setChosenVerbModalityParticle('');
            this.props.setChosenVerbSubordinatingSuffixParticle('');
            this.props.setChosenVerbPossessionMarkerParticle('');
            this.props.setChosenVerbCaseMarkerParticle('');
        });
    }

    setIPersonParticle() {
        let stringBeforeIPerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let iPersonParticle = '';
        if((this.props.chosenVerbTAM3 === 'Perfective' || this.props.chosenVerbTAM3 === 'If...') && (this.props.chosenVerbTAM4 === '')) { 
            this.props.setChosenVerbPersonParticle('m') 
        }
        else if(((this.props.chosenVerbTAM3 === 'Apparently...') || (this.props.chosenVerbTAM3 === 'If...') ||
            (this.props.chosenVerbTAM3 === 'Future')) || (this.props.chosenVerbTAM4 === 'Evidential...')) {
            iPersonParticle += 'y'
        }
        if(this.checkForVowelEnding(stringBeforeIPerson)) { iPersonParticle = 'm'; } else {
            switch(this.getLastVowelInString(stringBeforeIPerson)) {
                case 'a':
                case 'ı':
                    iPersonParticle = 'ım';
                    break;
                case 'o':
                case 'u':
                    iPersonParticle += 'um';
                    break;
                case 'e':
                case 'i':
                    iPersonParticle += 'im';
                    break;
                case 'ö':
                case 'ü':
                    iPersonParticle += 'üm';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        }
        this.props.setChosenVerbPersonParticle(iPersonParticle);
    }

    setYouSingularPersonParticle() {
        let stringBeforeYouSingularPerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let youSingularPersonParticle = '';
        if((this.props.chosenVerbTAM3 === 'Perfective' || this.props.chosenVerbTAM3 === 'If...') && (this.props.chosenVerbTAM4 === '')) { 
            this.setPersonParticle('n') 
        }
        if(this.checkForVowelEnding(stringBeforeYouSingularPerson)) { youSingularPersonParticle = 'n'; } else {
            switch(this.getLastVowelInString(stringBeforeYouSingularPerson)) {
                case 'a':
                case 'ı':
                    youSingularPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' : 'sın';
                    break;
                case 'o':
                case 'u':
                    youSingularPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'sun';
                    break;
                case 'e':
                case 'i':
                    youSingularPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'sin';
                    break;
                case 'ö':
                case 'ü':
                    youSingularPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'sün';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        }
        this.props.setChosenVerbPersonParticle(youSingularPersonParticle);
    }
    
    setHeSheItPersonParticle() {
        let stringBeforeHeSheItPerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let heSheItPersonParticle = '';
        if(this.props.chosenVerbTAM3 === "Let's...") {
            switch(this.getLastVowelInString(stringBeforeHeSheItPerson)) {
                case 'a':
                case 'ı':
                    heSheItPersonParticle = 'sın';
                    break;
                case 'o':
                case 'u':
                    heSheItPersonParticle += 'sun';
                    break;
                case 'e':
                case 'i':
                    heSheItPersonParticle += 'sin';
                    break;
                case 'ö':
                case 'ü':
                    heSheItPersonParticle += 'sün';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        }
        this.props.setChosenVerbPersonParticle(heSheItPersonParticle);
    }
    
    setWePersonParticle() {
        let stringBeforeWePerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let wePersonParticle = '';
        if((this.props.chosenVerbTAM3 === 'Perfective' || this.props.chosenVerbTAM3 === 'If...') && (this.props.chosenVerbTAM4 === '')) {       
            wePersonParticle = 'k'; 
        } else if(this.props.chosenVerbTAM3 !== "Let's...") {
            if(this.checkForVowelEnding(stringBeforeWePerson)) { wePersonParticle += 'y' }
            if(this.props.chosenVerbTAM3 === 'Future') { this.props.setChosenVerbTAM3Particle(this.props.chosenVerbTAM3Particle.replace('k', 'ğ'))}
            switch(this.getLastVowelInString(stringBeforeWePerson)) {
                case 'a':
                case 'ı':
                    wePersonParticle = 'ız';
                    break;
                case 'o':
                case 'u':
                    wePersonParticle += 'uz';
                    break;
                case 'e':
                case 'i':
                    wePersonParticle += 'iz';
                    break;
                case 'ö':
                case 'ü':
                    wePersonParticle += 'üz';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        } else {
            switch(this.getLastVowelInString(stringBeforeWePerson)) {
                case 'a':
                case 'ı':
                    wePersonParticle = 'lım';
                    break;
                case 'o':
                case 'u':
                    wePersonParticle += 'lum';
                    break;
                case 'e':
                case 'i':
                    wePersonParticle += 'lim';
                    break;
                case 'ö':
                case 'ü':
                    wePersonParticle += 'lüm';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        }
        this.props.setChosenVerbPersonParticle(wePersonParticle);
    }
    
    setYouPluralPersonParticle() {
        let stringBeforeYouPluralPerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let youPluralPersonParticle = '';
        if((this.props.chosenVerbTAM3 === 'Perfective' || this.props.chosenVerbTAM3 === 'If...') && (this.props.chosenVerbTAM4 === '')) {
            switch(this.getLastVowelInString(stringBeforeYouPluralPerson)) {
                case 'a':
                case 'ı':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' : 'nız';
                    break;
                case 'o':
                case 'u':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'nuz';
                    break;
                case 'e':
                case 'i':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'niz';
                    break;
                case 'ö':
                case 'ü':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'nüz';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }          
        } else {
            switch(this.getLastVowelInString(stringBeforeYouPluralPerson)) {
                case 'a':
                case 'ı':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' : 'sınız';
                    break;
                case 'o':
                case 'u':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'sunuz';
                    break;
                case 'e':
                case 'i':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'siniz';
                    break;
                case 'ö':
                case 'ü':
                    youPluralPersonParticle = this.props.chosenVerbVoice === 'Imperative' ? '' :'sünüz';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            }
        }
        this.props.setChosenVerbPersonParticle(youPluralPersonParticle);
    }
    
    setTheyPersonParticle() {
        let stringBeforeTheyPerson =
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle;
        let theyPersonParticle = '';
        if(this.props.chosenVerbTAM3 === "Let's...") {
            switch(this.getLastVowelInString(stringBeforeTheyPerson)) {
                case 'a':
                case 'ı':
                    theyPersonParticle = 'sınlar';
                    break;
                case 'o':
                case 'u':
                    theyPersonParticle += 'sunlar';
                    break;
                case 'e':
                case 'i':
                    theyPersonParticle += 'sinler';
                    break;
                case 'ö':
                case 'ü':
                    theyPersonParticle += 'sünler';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            } 
        } else {
            switch(this.getLastVowelInString(stringBeforeTheyPerson)) {
                case 'a':
                case 'ı':
                case 'o':
                case 'u':
                    theyPersonParticle += 'lar';
                    break;
                case 'e':
                case 'i':
                case 'ö':
                case 'ü':
                    theyPersonParticle += 'ler';
                    break;
                default:
                    this.props.setChosenVerbPersonParticle('');
            } 
        }
        this.props.setChosenVerbPersonParticle(theyPersonParticle);
    }

    setPersonParticle(_value) {
        switch(_value) {
            case "I":
                this.setIPersonParticle();
                break;
            case "you (s.)":
                this.setYouSingularPersonParticle();
                break;
            case "he/she/it":
                this.setHeSheItPersonParticle();
                break;
            case "we":
                this.setWePersonParticle();
                break;
            case "you (p.)":
                this.setYouPluralPersonParticle();
                break;
            case "they":
                this.setTheyPersonParticle();
                break;
            default:
                this.props.setChosenVerbPersonParticle('')
        }
        setTimeout(() => {
            this.setTAM2Particle(this.props.chosenVerbTAM2);
            this.setTAM3Particle(this.props.chosenVerbTAM3);
            this.setTAM4Particle(this.props.chosenVerbTAM4);
        });
    }
    
    handlePersonChoice(changeEvent) {
        this.props.setChosenVerbPerson(changeEvent.target.value);
        this.setPersonParticle(changeEvent.target.value);
        setTimeout(() => {
            this.props.setChosenVerbModality(false);
            this.props.setChosenVerbSubordinatingSuffix('');
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
            this.props.setChosenVerbModalityParticle('');
            this.props.setChosenVerbSubordinatingSuffixParticle('');
            this.props.setChosenVerbPossessionMarkerParticle('');
            this.props.setChosenVerbCaseMarkerParticle('');
        });
    }

    setModalityParticle() {
        if(this.props.chosenVerbModality) {
            let stringBeforeModality =
                this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle + this.props.chosenVerbPersonParticle;
            let modalityParticle = '';
            if(this.checkForVoicelessEndingConsonant(stringBeforeModality)) { modalityParticle += 't' } else { modalityParticle += 'd' }
            switch(this.getLastVowelInString(stringBeforeModality)) {
                case 'a':
                case 'ı':
                    modalityParticle += 'ır';
                    break;
                case 'o':
                case 'u':
                    modalityParticle += 'ur';
                    break;
                case 'e':
                case 'i':
                    modalityParticle += 'ir';
                    break;
                case 'ö':
                case 'ü':
                    modalityParticle += 'ür';
                    break;
                default:
                    this.props.setChosenVerbModalityParticle('');
            }
            this.props.setChosenVerbModalityParticle(modalityParticle);
        }
    }

    handleModalityChoice(changeEvent) {
        if(changeEvent.target.checked) {
            this.props.setChosenVerbModality(true);
        } else {
            this.props.setChosenVerbModality(false);
        }
        setTimeout(() => {
            this.setModalityParticle();
            this.props.setChosenVerbSubordinatingSuffix('');
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
            this.props.setChosenVerbSubordinatingSuffixParticle('');
            this.props.setChosenVerbPossessionMarkerParticle('');
            this.props.setChosenVerbCaseMarkerParticle('');
        });
    }

    setTheFactThatParticle() {
        let stringBeforeTheFactThat = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let theFactThatPartice = '';
        if(this.checkForVoicelessEndingConsonant(stringBeforeTheFactThat)) { theFactThatPartice += 't' } else { theFactThatPartice += 'd' }
        switch(this.getLastVowelInString(stringBeforeTheFactThat)) {
            case 'a':
            case 'ı':
                theFactThatPartice = 'ığ';
                break;
            case 'o':
            case 'u':
                theFactThatPartice += 'uğ';
                break;
            case 'e':
            case 'i':
                theFactThatPartice += 'iğ';
                break;
            case 'ö':
            case 'ü':
                theFactThatPartice += 'üğ';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(theFactThatPartice);
    }
    
    setTheProspectOfParticle() {
        let stringBeforeTheProspectOf = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let theProspectOfParticle = '';
        if(this.checkForVowelEnding(stringBeforeTheProspectOf)) { theProspectOfParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeTheProspectOf)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                theProspectOfParticle += 'acak';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                theProspectOfParticle += 'ecek';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        if(this.props.chosenVerbPossessionMarker !== '') {
            if(['a', 'e', 'ı', 'i', 'o', 'ö', 'u', 'ü'].includes(this.props.chosenVerbPossessionMarkerParticle[0]))
                { theProspectOfParticle = theProspectOfParticle.replace('k', 'ğ'); }
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(theProspectOfParticle);
    }
    
    setBasicGerundParticle() {
        let stringBeforeBasicGerund = 
        this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let basicGerundParticle = '';
        switch(this.getLastVowelInString(stringBeforeBasicGerund)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                basicGerundParticle += 'ma';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                basicGerundParticle += 'me';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(basicGerundParticle);
    }
    
    setPresentParticipleParticle() {
        let stringBeforePresentParticiple = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let presentParticipleParticle = '';
        if(this.checkForVowelEnding(stringBeforePresentParticiple)) { presentParticipleParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforePresentParticiple)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                presentParticipleParticle += 'an';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                presentParticipleParticle += 'en';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(presentParticipleParticle);
    }
    
    setWayOfParticle() {
        let stringBeforeWayOf = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let wayOfParticle = '';
        if(this.checkForVowelEnding(stringBeforeWayOf)) { wayOfParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeWayOf)) {
            case 'a':
            case 'ı':
                wayOfParticle = 'ış';
                break;
            case 'o':
            case 'u':
                wayOfParticle += 'uş';
                break;
            case 'e':
            case 'i':
                wayOfParticle += 'iş';
                break;
            case 'ö':
            case 'ü':
                wayOfParticle += 'üş';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(wayOfParticle);
    }
    
    setBeforeBeingParticle() {
        let stringBeforeBeforeBeing = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let beforeBeingParticle = '';
        switch(this.getLastVowelInString(stringBeforeBeforeBeing)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                beforeBeingParticle += 'madan';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                beforeBeingParticle += 'meden';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(beforeBeingParticle);
    }
    
    setWhenXingarticle() {
        let stringBeforeWhenXing = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let whenXingParticle = '';
        if(this.checkForVowelEnding(stringBeforeWhenXing)) { whenXingParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeWhenXing)) {
            case 'a':
            case 'ı':
                whenXingParticle = 'ınca';
                break;
            case 'o':
            case 'u':
                whenXingParticle += 'unca';
                break;
            case 'e':
            case 'i':
                whenXingParticle += 'ince';
                break;
            case 'ö':
            case 'ü':
                whenXingParticle += 'ünce';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(whenXingParticle);
    }
    
    setSinceXingParticle() {
        let stringBeforeSinceXing = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let sinceXingParticle = '';
        if(this.checkForVowelEnding(stringBeforeSinceXing)) { sinceXingParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeSinceXing)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                sinceXingParticle += 'alı beri';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                sinceXingParticle += 'eli beri';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(sinceXingParticle);
    }
    
    setByTheTimeParticle() {
        let stringBeforeByTheTime = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let byTheTimeParticle = '';
        if(this.checkForVowelEnding(stringBeforeByTheTime)) { byTheTimeParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeByTheTime)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                byTheTimeParticle += 'ıncaya kadar/değin/dek';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                byTheTimeParticle += 'inceye kadar/değin/dek';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(byTheTimeParticle);
    }
    
    setAsIfParticle() {
        let stringBeforeAsIf = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle;
        let asIfParticle = '';
        if(this.checkForVowelEnding(stringBeforeAsIf)) { asIfParticle += 'y' }
        switch(this.getLastVowelInString(stringBeforeAsIf)) {
            case 'a':
            case 'ı':
                asIfParticle = 'mış[?]çasına';
                break;
            case 'o':
            case 'u':
                asIfParticle += 'muş[?]çasına';
                break;
            case 'e':
            case 'i':
                asIfParticle += 'miş[?]çesine';
                break;
            case 'ö':
            case 'ü':
                asIfParticle += 'müş[?]çesine';
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('');
        }
        this.props.setChosenVerbSubordinatingSuffixParticle(asIfParticle);
    }

    setSubordinatingSuffixParticle(_value) {
        switch(_value) {
            case "The fact that...":
                this.setTheFactThatParticle();
                break;
            case "The prospect of...":
                this.setTheProspectOfParticle();
                break;
            case "Basic gerund":
                this.setBasicGerundParticle();
                break;
            case "Present participle":
                this.setPresentParticipleParticle();
                break;
            case "Way of...":
                this.setWayOfParticle();
                break;
            case "Before being...":
                this.setBeforeBeingParticle();
                break;
            case "When ...ing":
                this.setWhenXingarticle();
                break;
            case "Since ...ing":
                this.setSinceXingParticle();
                break;
            case "By the time...":
                this.setByTheTimeParticle();
                break;
            case "As if ...ing":
                this.setAsIfParticle();
                break;
            default:
                this.props.setChosenVerbSubordinatingSuffixParticle('')
        }
    }

    handleSubordinatingSuffixChoice(changeEvent) {
        if(changeEvent.target.value !== 'As if ...ing') {
            this.props.setChosenVerbPossessionMarker('');
            this.props.setChosenVerbCaseMarker('');
        }
        this.props.setChosenVerbSubordinatingSuffix(changeEvent.target.value);
        this.setSubordinatingSuffixParticle(changeEvent.target.value);
        this.props.setChosenVerbPossessionMarker('');
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbPossessionMarkerParticle('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }

    setMyParticle() {
        let stringBeforeMy = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let myParticle = '';
        switch(this.getLastVowelInString(stringBeforeMy)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışımçasına"); } else { myParticle += 'ım'; } 
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşumçasına"); } else {  myParticle += 'um'; }  
                break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişimçesine"); } else { myParticle += 'im'; }
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşümçesine"); } else { myParticle += 'üm'; } 
                break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(myParticle);
    }

    setYourSingularParticle() {
        let stringBeforeYourSingular = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let yourSingularParticle = '';
        switch(this.getLastVowelInString(stringBeforeYourSingular)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışınçasına"); } else { yourSingularParticle += 'ın'; } 
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşunçasına"); } else { yourSingularParticle += 'un'; } 
                    break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') { 
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişinçesine"); } else { yourSingularParticle += 'in'; } 
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşünçesine"); } else { yourSingularParticle += 'ün'; } 
                    break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(yourSingularParticle);
    }

    setHisHerItsParticle() {
        let stringBeforeHisHerIts = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let hisHerItsParticle = '';
        if(this.checkForVowelEnding(stringBeforeHisHerIts)) { hisHerItsParticle += 's' }
        switch(this.getLastVowelInString(stringBeforeHisHerIts)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışıçasına"); } else { hisHerItsParticle += 'ı'; } 
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşuçasına"); } else { hisHerItsParticle += 'u'; } 
                break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişiçesine"); } else { hisHerItsParticle += 'i'; } 
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşüçesine"); } else { hisHerItsParticle += 'ü'; } 
                break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(hisHerItsParticle);
    }

    setOurParticle() {
        let stringBeforeOur = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let ourParticle = '';
        switch(this.getLastVowelInString(stringBeforeOur)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışımızcasına"); }
                    else { ourParticle = this.checkForVowelEnding(stringBeforeOur) ? 'mız' : 'ımız'; } 
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşumuzcasına"); }
                    else { ourParticle = this.checkForVowelEnding(stringBeforeOur) ? 'muz' : 'umuz'; } 
                break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişimizcesine"); }
                    else { ourParticle = this.checkForVowelEnding(stringBeforeOur) ? 'miz' : 'imiz'; } 
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşümüzcesine"); }
                    else { ourParticle = this.checkForVowelEnding(stringBeforeOur) ? 'müz' : 'ümüz'; } 
                    break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(ourParticle);
    }

    setYourPluralParticle() {
        let stringBeforeYourPlural = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let yourPluralParticle = '';
        switch(this.getLastVowelInString(stringBeforeYourPlural)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışınızcasına"); }
                    else { yourPluralParticle = this.checkForVowelEnding(stringBeforeYourPlural) ? 'nız' : 'ınız'; } 
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşunuzcasına"); }
                    else { yourPluralParticle = this.checkForVowelEnding(stringBeforeYourPlural) ? 'nuz' : 'unuz'; } 
                break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişinizcesine"); }
                    else { yourPluralParticle = this.checkForVowelEnding(stringBeforeYourPlural) ? 'niz' : 'iniz'; } 
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşünüzcesine"); }
                    else { yourPluralParticle = this.checkForVowelEnding(stringBeforeYourPlural) ? 'nüz' : 'ünüz'; } 
                break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(yourPluralParticle);
    }

    setTheirParticle() {
        let stringBeforeTheir = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle;
        let theirParticle = '';
        switch(this.getLastVowelInString(stringBeforeTheir)) {
            case 'a':
            case 'ı':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mışlarıçasına"); } else { theirParticle = 'ları'; }
                break;
            case 'o':
            case 'u':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("muşlarıçasına"); } else { theirParticle = 'ları'; }
                break;
            case 'e':
            case 'i':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("mişleriçesıne"); } else { theirParticle = 'leri'; }
                break;
            case 'ö':
            case 'ü':
                if(this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') {
                    this.props.setChosenVerbSubordinatingSuffixParticle("müşleriçesıne"); } else { theirParticle = 'leri'; }
                break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('');
        }
        this.props.setChosenVerbPossessionMarkerParticle(theirParticle);
    }

    setPossessionMarkerParticle(_value) {
        switch(_value) {
            case "my":
                this.setMyParticle();
                break;
            case "your (s.)":
                this.setYourSingularParticle();
                break;
            case "his/her/its":
                this.setHisHerItsParticle();
                break;
            case "our":
                this.setOurParticle();
                break;
            case "your (p.)":
                this.setYourPluralParticle();
                break;
            case "their":
                this.setTheirParticle();
                break;
            default:
                this.props.setChosenVerbPossessionMarkerParticle('')
        }
        setTimeout(() => {
            this.setSubordinatingSuffixParticle(this.props.chosenVerbSubordinatingSuffix);
        });
    }

    handlePossessionMarkerChoice(changeEvent) {
        this.props.setChosenVerbPossessionMarker(changeEvent.target.value);
        this.setPossessionMarkerParticle(changeEvent.target.value);
        this.props.setChosenVerbCaseMarker('');
        this.props.setChosenVerbCaseMarkerParticle('');
    }

    setAccusativeParticle() {
        let stringBeforeAccusative = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let accusativeParticle = '';
        if(this.checkForVowelEnding(stringBeforeAccusative)) { accusativeParticle += 'n'; }
        switch(this.getLastVowelInString(stringBeforeAccusative)) {
            case 'a':
            case 'ı':
                accusativeParticle += 'ı';
                break;
            case 'o':
            case 'u':
                accusativeParticle += 'u';
                break;
            case 'e':
            case 'i':
                accusativeParticle += 'i';
                break;
            case 'ö':
            case 'ü':
                accusativeParticle += 'ü';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(accusativeParticle);
    }
    
    setDativeParticle() {
        let stringBeforeDative = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let dativeParticle = '';
        if(this.checkForVowelEnding(stringBeforeDative)) { dativeParticle += 'n'; }
        switch(this.getLastVowelInString(stringBeforeDative)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                dativeParticle += 'a';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                dativeParticle += 'e';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(dativeParticle);
    }
    
    setGenitiveParticle() {
        let stringBeforeGenitive = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let genitiveParticle = '';
        if(this.checkForVowelEnding(stringBeforeGenitive)) { genitiveParticle += 'n'; }
        switch(this.getLastVowelInString(stringBeforeGenitive)) {
            case 'a':
            case 'ı':
                genitiveParticle += 'ın';
                break;
            case 'o':
            case 'u':
                genitiveParticle += 'un';
                break;
            case 'e':
            case 'i':
                genitiveParticle += 'in';
                break;
            case 'ö':
            case 'ü':
                genitiveParticle += 'ün';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(genitiveParticle);
    }
    
    setLocativeParticle() {
        let stringBeforeLocative = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let locativeParticle = '';
        if(this.checkForVowelEnding(stringBeforeLocative)) { locativeParticle += 'n'; }
        if(this.checkForVoicelessEndingConsonant(stringBeforeLocative)) { locativeParticle += 't' } else { locativeParticle += 'd' }
        switch(this.getLastVowelInString(stringBeforeLocative)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                locativeParticle += 'a';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                locativeParticle += 'e';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(locativeParticle);
    }
    
    setAblativeParticle() {
        let stringBeforeAblative = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let ablativeParticle = '';
        if(this.checkForVowelEnding(stringBeforeAblative)) { ablativeParticle += 'n'; }
        if(this.checkForVoicelessEndingConsonant(stringBeforeAblative)) { ablativeParticle += 't' } else { ablativeParticle += 'd' }
        switch(this.getLastVowelInString(stringBeforeAblative)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                ablativeParticle += 'an';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                ablativeParticle += 'en';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(ablativeParticle);
    }
    
    setComitativeParticle() {
        let stringBeforeComitative = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle;
        let comitativeParticle = '';
        if(this.checkForVowelEnding(stringBeforeComitative)) { comitativeParticle += 'y'; }
        switch(this.getLastVowelInString(stringBeforeComitative)) {
            case 'a':
            case 'ı':
            case 'o':
            case 'u':
                comitativeParticle += 'la';
                break;
            case 'e':
            case 'i':
            case 'ö':
            case 'ü':
                comitativeParticle += 'le';
                break;
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
        }
        this.props.setChosenVerbCaseMarkerParticle(comitativeParticle);
    }

    setCaseMarkerParticle(_value) {
        switch(_value) {
            case "Nominative":
            default:
                this.props.setChosenVerbCaseMarkerParticle('');
                break;
            case "Accusative":
                this.setAccusativeParticle();
                break;
            case "Dative":
                this.setDativeParticle();
                break;
            case "Genitive":
                this.setGenitiveParticle();
                break;
            case "Locative":
                this.setLocativeParticle();
                break;
            case "Ablative":
                this.setAblativeParticle();
                break;
            case "Comitative":
                this.setComitativeParticle();
                break;
        }
        this.setSubordinatingSuffixParticle(this.props.chosenVerbSubordinatingSuffix);
        this.setPossessionMarkerParticle(this.props.chosenVerbPossessionMarker);
    }

    handleCaseMarkerChoice(changeEvent) {
        this.props.setChosenVerbCaseMarker(changeEvent.target.value);
        this.setCaseMarkerParticle(changeEvent.target.value);
    }

    getAnyEnglishIntro() {
        if((this.props.chosenVerbTAM3 === 'If...') || (this.props.chosenVerbTAM4 === 'Conditional_Copula')) { return 'If'; }
        else if(this.props.chosenVerbModality) { return 'Presumably,'}
        else if((this.props.chosenVerbTAM3 === 'Apparently...') || (this.props.chosenVerbTAM3 === 'Evidential_Copula')) { return 'Apparently, '; }
        else if(this.props.chosenVerbInability && (this.props.chosenVerbTAM3 === '') && (this.props.chosenVerbTAM4 === '')) {
            return 'not be able to';
        }
        else if(this.props.chosenVerbNegative && !this.props.chosenVerbInability && (this.props.chosenVerbTAM3 === '') && (this.props.chosenVerbTAM4 === '')) { return 'not'; }
        else { return ''; }
    }

    getEnglishModal() {
        if(this.props.chosenVerbInability) {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula')))
                { return "couldn't"; }
            else if(this.props.chosenVerbTAM3 === 'Future') { return "won't be able to"; }
            else if(['Aorist', 'If...'].includes(this.props.chosenVerbTAM3) || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 !== 'Past_Copula'))){ return "can't"; }
            else { return ''; }
        } else if(this.chosenVerbNegative && !this.props.chosenVerbInability) {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula')))
            { return "didn't"; }
            else if(this.props.chosenVerbTAM3 === 'Future') { return "won't"; }
            else { 
                if(this.props.chosenVerbPerson === 'he/she/it') { return "doesn't" } else { return "don't" }
            }
        } else if(this.props.chosenVerbTAM2 === 'Ability') {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula')))
                { return "could"; }
            else if(this.props.chosenVerbTAM3 === 'Future') { return "will be able to"; }
            else { return "can"; }
        } else if(this.props.chosenVerbTAM2 === 'Happen to...') {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula')))
                { return "managed to"; }
            else if(this.props.chosenVerbTAM3 === 'Future') { return "will manage to"; }
            else { 
                if(this.props.chosenVerbPerson === 'he/she/it') { return "manages to" } else { return "manage to" }
            }
        } else if(this.props.chosenVerbTAM2 === 'Keep on...') {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula')))
                { return "kept on"; }
            else if(this.props.chosenVerbTAM3 === 'Future') { return "will keep on"; }
            else { 
                if(this.props.chosenVerbPerson === 'he/she/it') { return "keeps on" } else { return "keep on" }
            }
        } else if(this.props.chosenVerbTAM2 === 'Almost') {
            return 'almost';
        } else { return ''; }
    }

    getEnglishExtraCausativeMarker() {
        if(this.props.chosenVerbExtraCausative) {
            if(this.props.chosenVerbTAM3 === 'Perfective') { return 'caused sb./st. to'; }
            else if (this.props.chosenVerbTAM3 === 'Imperfective') {
                if(this.props.chosenVerbTAM4 === 'Past_Copula') {
                    if(['I', 'he/she/it'].includes(this.props.chosenVerbPerson)) {
                        return 'was causing sb./st. to';
                    } else { return 'were causing sb./st. to'; }
                } else {
                    if(this.props.chosenVerbPerson === 'he/she/it') {
                        return 'is causing sb./st. to';
                    } else if(this.props.chosenVerbPerson === 'I') { return 'am causing sb./st. to'; }
                    else { return 'are causing sb./st. to'; }

                }
            }
            else if (this.props.chosenVerbTAM3 === 'Future') {
                return 'will cause sb./st. to';
            } else if (this.props.chosenVerbTAM3 === 'Aorist') {
                if(this.props.chosenVerbPerson === 'he/she/it') {
                    return 'causes sb./st. to';
                } else { return 'cause sb./st. to'; }
            } else { return 'cause sb./st. to'; }
        } else { return ''; }
    }

    getEnglishPassiveMarker() {
        if(this.props.chosenVerbVoice === 'Passive') {
            if((this.props.chosenVerbTAM3 === 'Perfective') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula'))) {
                if(this.props.chosenVerbNegative) {
                    if(this.props.chosenVerbInability) { return 'be' } else {
                        if(['we', 'you (p.)', 'they'].includes(this.props.chosenVerbPerson)) { return "weren't"; } else { return "wasn't"; }
                    }
                } else {
                    if(['we', 'you (p.)', 'they'].includes(this.props.chosenVerbPerson)) { return "were"; } else { return "was"; }
                }
            }
            else if(this.props.chosenVerbTAM3 === 'Future') {
                if(this.props.chosenVerbNegative) {
                    if(this.props.chosenVerbInability) { return 'be' } else { return "won't"; }
                } else {
                    return "will be";
                }
            }
            else if((this.props.chosenVerbTAM3 === 'Aorist') || (['If...', 'Imperfective'].includes(this.props.chosenVerbTAM3) && (this.props.chosenVerbTAM4 !== 'Past_Copula'))){
                if(this.props.chosenVerbNegative) {
                    if(this.props.chosenVerbInability) { return 'be' } else {
                        if(['we', 'you (s.)', 'you (p.)', 'they'].includes(this.props.chosenVerbPerson)) { return "aren't"; }
                        else if(this.props.chosenVerbPerson === 'I') { return 'am not'; } else { return "isn't"; }
                    }
                } else {
                    if(['we', 'you (s.)', 'you (p.)', 'they'].includes(this.props.chosenVerbPerson)) { return 'are'; }
                    else if(this.props.chosenVerbPerson === 'I') { return 'am'; } else { return 'is'; }
                } 
            } else { return 'be'; }
        } else { return ''; }
    }

    getEnglishCausativeMarker() {
        if(this.props.chosenVerbVoice === 'Causative') {
            if(this.props.chosenVerbTAM3 === 'Perfective') { return 'caused sb./st. to'; }
            else if (this.props.chosenVerbTAM3 === 'Imperfective') {
                if(this.props.chosenVerbTAM4 === 'Past_Copula') {
                    if(['I', 'he/she/it'].includes(this.props.chosenVerbPerson)) {
                        return 'was causing sb./st. to';
                    } else { return 'were causing sb./st. to'; }
                } else {
                    if(this.props.chosenVerbPerson === 'he/she/it') {
                        return 'is causing sb./st. to';
                    } else if(this.props.chosenVerbPerson === 'I') { return 'am causing sb./st. to'; }
                    else { return 'are causing sb./st. to'; }

                }
            }
            else if (this.props.chosenVerbTAM3 === 'Future') {
                return 'will cause sb./st. to';
            } else { return 'cause to'; }
        } else { return ''; } 
    }

    getEnglishTenseMarker() {
        if((this.props.chosenVerbTAM3 === 'Future') && !this.props.chosenVerbInability) {
            if(!this.props.chosenVerbNegative) { return 'will'; } else { return "won't"; }
        } else { return ''; }
    }

    getEnglishCase() {
        if(this.props.chosenVerbFiniteness === 'Subordinate') {
            switch(this.props.chosenVerbCaseMarker) {
                case 'Nominative':
                case 'Accusative':
                    return '';
                case 'Dative': return 'to'; 
                case 'Genitive': return 'of'; 
                case 'Locative': return 'in'; 
                case 'Ablative': return 'from';
                case 'Comitative': return 'with';
                default: return '';
            }
        } else { return ''; }
    }

    getEnglishMainVerb() {
        if((this.props.chosenVerbVoice === 'Causative') || this.props.chosenVerbInability || (this.props.chosenVerbNegative && !this.props.chosenVerbInability) || (this.props.chosenVerbTAM2 === 'Happen to...') || (this.props.chosenVerbTAM2 === 'Ability') || (this.props.chosenVerbTAM3 === 'Future')) {
            return this.props.chosenVerbEnglish.replace('to ', '');
        } else if((this.props.chosenVerbTAM2 === 'Keep on...') || (this.props.chosenVerbFiniteness === 'Subordinate') || ((this.props.chosenVerbTAM3 === 'Imperfective') && (this.props.chosenVerbTAM4 === 'Past_Copula'))) { return this.chosenVerbEnglishGerund; }
        else if(this.props.chosenVerbTAM3 === 'Perfective') {
            if(['you (s.)', 'we', 'you (p.)', 'they'].includes(this.props.chosenVerbPerson)) {
                return !this.props.chosenVerbEnglishThirdPersonPluralPast ?
                    this.props.chosenVerbEnglishPast : this.props.chosenVerbEnglishThirdPersonPluralPast; 
            } else { return this.props.chosenVerbEnglishPast; } 
        } else if(this.props.chosenVerbVoice === 'Passive') { return this.props.chosenVerbEnglishPastParticiple; }
        else {
            if(this.props.chosenVerbPerson === 'he/she/it') { return this.props.chosenVerbEnglishThirdPersonSingularPresent; }
            else { return this.props.chosenVerbEnglishThirdPersonPluralPresent; }
        }
    }

    getEnglishReflexiveOrReciprocal() {
        if(this.props.chosenVerbVoice === 'Reflexive') {
            switch(this.props.chosenVerbPerson) {
                case 'I':
                    return 'myself';
                case 'you (s.)':
                    return 'yourself';
                case 'he/she/it':
                    return 'himself/herself/itself';
                case 'we':
                    return 'ourselves';
                case 'you (p.)':
                    return 'yourselves';
                case 'they':
                    return 'themselves';
                default:
                    return 'itself';
            }
        }
        else if(this.props.chosenVerbVoice === 'Reciprocal') {
            return 'each other';
        } else { return ''; }
    }

    translate() {
        let _translation = ''
        switch(this.props.chosenVerbFiniteness){
            case 'Finite':
            default:
                _translation =
                    this.getAnyEnglishIntro() + ' ' + this.props.chosenVerbPerson + ' ' + this.getEnglishExtraCausativeMarker() + ' ' + (this.props.chosenVerbVoice === 'Passive' ? this.getEnglishPassiveMarker() : '') + ' ' + this.getEnglishModal() + ' ' + (this.props.chosenVerbVoice !== 'Passive' ? this.getEnglishPassiveMarker() : '') + ' ' + this.getEnglishCausativeMarker() + ' ' + this.getEnglishTenseMarker() + ' ' + this.getEnglishMainVerb() + ' ' + this.getEnglishReflexiveOrReciprocal();
                    break;
            case 'Subordinate':
                _translation =
                    this.getEnglishCase() + ' ' + this.props.chosenVerbPossessionMarker + ' ' + this.getEnglishExtraCausativeMarker() + ' ' + this.getEnglishPassiveMarker() + ' ' + this.getEnglishCausativeMarker() + ' ' + this.getEnglishMainVerb() + ' ' + this.getEnglishReflexiveOrReciprocal();
                    break;
        }
        this.setState({ translation: _translation });
    }
    
    render() {

        let voiceContent =
            <Form>
                <FormGroup>
                    <Label for="radios"><small>Click on one of the options below: -</small></Label>
                    <div>
                        <CustomInput type="radio" id="activeVoiceRadio" name="voiceRadio" label="Active" value="Active"
                            checked={this.props.chosenVerbVoice === 'Active'} onChange={this.handleVoiceChoice}></CustomInput>
                        <CustomInput type="radio" id="causativeVoiceRadio" name="voiceRadio" label="Causative" value="Causative"
                            checked={this.props.chosenVerbVoice === 'Causative'} onChange={this.handleVoiceChoice}></CustomInput>
                        {this.props.chosenVerbTurkishPassiveAvailable ?
                            <CustomInput type="radio" id="passiveVoiceRadio" name="voiceRadio" label="Passive" value="Passive"
                                checked={this.props.chosenVerbVoice === 'Passive'} onChange={this.handleVoiceChoice}></CustomInput> : null}
                        {this.props.chosenVerbTurkishPassiveAvailable ?
                            <CustomInput type="radio" id="reflexiveVoiceRadio" name="voiceRadio" label="Reflexive" value="Reflexive"
                                checked={this.props.chosenVerbVoice === 'Reflexive'} onChange={this.handleVoiceChoice}></CustomInput> : null}
                        {this.props.chosenVerbTurkishPassiveAvailable ?
                            <CustomInput type="radio" id="reciprocalVoiceRadio" name="voiceRadio" label="Reciprocal" value="Reciprocal"
                                checked={this.props.chosenVerbVoice === 'Reciprocal'} onChange={this.handleVoiceChoice}></CustomInput> : null}
                    </div>
                </FormGroup>
                <FormGroup>
                    <div>
                        <CustomInput disabled={(this.props.chosenVerbVoice === '') || (this.props.chosenVerbVoice === 'Active')} type="checkbox" id="extraCausativeCheckbox" name="extraCausativeCheckbox" label="Extra Causative" onChange={this.handleExtraCausativeChoice} checked={this.props.chosenVerbExtraCausative}/>
                    </div>
                </FormGroup>
            </Form>;

        let negativeContent = 
            <Form>
                <FormGroup>
                    <Label for="radios"><small>Click to add the negative element (if not already triggered by Inability): -</small></Label>
                    <div>
                        <CustomInput type="radio" id="negativeYesRadio" name="negativeRadio" label="Yes" value="Yes"
                            checked={this.props.chosenVerbNegative} onChange={this.handleNegativeChoice}></CustomInput>
                        <CustomInput type="radio" id="negativeNoRadio" name="negativeRadio" label="No" value="No"
                                disabled={this.props.chosenVerbInability} checked={!this.props.chosenVerbNegative} onChange={this.handleNegativeChoice}></CustomInput>
                    </div>
                </FormGroup>
                <FormGroup>
                    <div>
                        <CustomInput type="checkbox" id="inabilityCheckbox" label="Inability" onChange={this.handleInabilityChoice}/>
                    </div>
                </FormGroup>
            </Form>;

        let finitenessContent = 
            <Form>
                <FormGroup>
                    <div>
                        <CustomInput type="radio" id="finiteRadio" name="finitenessRadio" label="Main Sentence Verb" value="Finite"
                            checked={this.props.chosenVerbFiniteness === 'Finite'} onChange={this.handleFinitenessChoice}></CustomInput>
                        <CustomInput type="radio" id="subordinatingRadio" name="finitenessRadio" label="Subordinate Verb" value="Subordinate"
                                checked={this.props.chosenVerbFiniteness === 'Subordinate'} onChange={this.handleFinitenessChoice}></CustomInput>
                    </div>
                </FormGroup>
            </Form>;

        let tam2Content = 
            <Form>
                <FormGroup>
                    <div>
                        <CustomInput type="radio" id="abilityRadio" name="tam2Radio" label="Ability" value="Ability"
                            checked={this.props.chosenVerbTAM2 === 'Ability'} onChange={this.handleTAM2Choice}></CustomInput>
                        <CustomInput type="radio" id="happenToRadio" name="tam2Radio" label="Happen to..." value="Happen to..."
                            checked={this.props.chosenVerbTAM2 === 'Happen to...'} onChange={this.handleTAM2Choice}></CustomInput>
                        <CustomInput type="radio" id="keepOnRadio" name="tam2Radio" label="Keep on..." value="Keep on..."
                            checked={this.props.chosenVerbTAM2 === 'Keep on...'} onChange={this.handleTAM2Choice}></CustomInput>
                        <CustomInput type="radio" id="almostRadio" name="tam2Radio" label="Almost..." value="Almost"
                            checked={this.props.chosenVerbTAM2 === 'Almost'} onChange={this.handleTAM2Choice}></CustomInput>
                        <CustomInput type="radio" id="noTAM2Radio" name="tam2Radio" label="None" value=""
                            checked={this.props.chosenVerbTAM2 === ''} onChange={this.handleTAM2Choice}></CustomInput>
                    </div>
                </FormGroup>
            </Form>;

        let tam3Content = 
            <Form>
                <FormGroup>
                    <div>
                        <CustomInput type="radio" id="perfectiveRadio" name="tam3Radio" label="Perfective" value="Perfective"
                            checked={this.props.chosenVerbTAM3 === 'Perfective'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="evidentialRadio" name="tam3Radio" label="Apparently..." value="Apparently..."
                            checked={this.props.chosenVerbTAM3 === 'Apparently...'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="conditionalRadio" name="tam3Radio" label="If..." value="If..."
                            checked={this.props.chosenVerbTAM3 === 'If...'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="aoristRadio" name="tam3Radio" label="Aorist" value="Aorist"
                            checked={this.props.chosenVerbTAM3 === 'Aorist'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="futureRadio" name="tam3Radio" label="Future" value="Future"
                            checked={this.props.chosenVerbTAM3 === 'Future'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="imperfectiveRadio" name="tam3Radio" label="Imperfective" value="Imperfective"
                            checked={this.props.chosenVerbTAM3 === 'Imperfective'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="obligativeRadio" name="tam3Radio" label="Must..." value="Must..."
                            checked={this.props.chosenVerbTAM3 === 'Must...'} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="optativeRadio" name="tam3Radio" label="Let's..." value="Let's..."
                            checked={this.props.chosenVerbTAM3 === "Let's..."} onChange={this.handleTAM3Choice}></CustomInput>
                        <CustomInput type="radio" id="noTAM3Radio" name="tam3Radio" label="None" value=""
                            checked={this.props.chosenVerbTAM3 === ''} onChange={this.handleTAM3Choice}></CustomInput>
                    </div>
                </FormGroup>
            </Form>;

        let tam4Content = 
            <Form>
                <FormGroup>
                    <Label for="radios"><small>Click to add a copula:</small></Label>
                    <div>
                        <CustomInput type="radio" id="pastCopulaRadio" name="tam4Radio" label="Past" value="Past_Copula"
                            disabled={(this.props.chosenVerbTAM3 === 'Perfective') || (this.props.chosenVerbTAM3 === "Let's...") || (this.props.chosenVerbTAM3 === "Future")} checked={this.props.chosenVerbTAM4 === 'Past_Copula'} onChange={this.handleTAM4Choice}></CustomInput>
                        <CustomInput type="radio" id="evidentialCopulaRadio" name="tam4Radio" label="Evidential" value="Evidential_Copula"
                            disabled={((this.props.chosenVerbTAM3 === 'Perfective') || (this.props.chosenVerbTAM3 === "Let's...") ) || this.props.chosenVerbTAM3 === 'Apparently...' } checked={this.props.chosenVerbTAM4 === 'Evidential_Copula'} onChange={this.handleTAM4Choice}></CustomInput>
                        <CustomInput type="radio" id="conditionalCopulaRadio" name="tam4Radio" label="Conditional" value="Conditional_Copula"
                            disabled={(this.props.chosenVerbTAM3 === 'If...')  || (this.props.chosenVerbTAM3 === "Let's...")} checked={this.props.chosenVerbTAM4 === 'Conditional_Copula'} onChange={this.handleTAM4Choice}></CustomInput>
                        <CustomInput type="radio" id="noTAM42Radio" name="tam4Radio" label="None" value=""
                            checked={this.props.chosenVerbTAM4 === ''} onChange={this.handleTAM4Choice}></CustomInput>
                    </div>
                </FormGroup>
            </Form>;

        let personContent = 
            <Form>
                <FormGroup>
                    <Label for="radios"><small>Click to add the person or "actor" of the verb:</small></Label>
                    <div>
                        <CustomInput type="radio" id="iRadio" name="personRadio" label="I" value="I"
                            disabled={this.props.chosenVerbVoice === "Imperative"} checked={this.props.chosenVerbPerson === 'I'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="yousRadio" name="personRadio" label="you (s.)" value="you (s.)"
                            checked={this.props.chosenVerbPerson === 'you (s.)'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="hesheitRadio" name="personRadio" label="he/she/it" value="he/she/it"
                            checked={this.props.chosenVerbPerson === 'he/she/it'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="weRadio" name="personRadio" label="we" value="we"
                            disabled={this.props.chosenVerbVoice === "Imperative"} checked={this.props.chosenVerbPerson === 'we'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="youpRadio" name="personRadio" label="you (p.)" value="you (p.)"
                            disabled={this.props.chosenVerbVoice === "Imperative"} checked={this.props.chosenVerbPerson === 'you (p.)'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="theyRadio" name="personRadio" label="they" value="they"
                            disabled={this.props.chosenVerbVoice === "Imperative"} checked={this.props.chosenVerbPerson === 'they'} onChange={this.handlePersonChoice}></CustomInput>
                        <CustomInput type="radio" id="noPersonRadio" name="personRadio" label="None" value=""
                            checked={this.props.chosenVerbPerson === ''} onChange={this.handlePersonChoice}></CustomInput>
                    </div>
                </FormGroup>
                <FormGroup>
                    <div>
                        <CustomInput disabled={(this.props.chosenVerbTAM3 === 'Perfective') || (this.props.chosenVerbTAM3 === 'If...') || (this.props.chosenVerbTAM3 === 'Aorist') || (this.props.chosenVerbVoice === "Imperative") || (this.props.chosenVerbPerson === "")} type="checkbox" id="modalityCheckbox" label="Add Generalising Modality" onChange={this.handleModalityChoice} checked={this.props.chosenVerbModality}/>
                    </div>
                </FormGroup>
            </Form>;                            

        let subordinatingSuffixContent = 
            <Form>
                <FormGroup>
                    <div>
                        <CustomInput type="radio" id="theFactThatRadio" name="subordinatingSuffixRadio" label="The fact that..."
                            value="The fact that..." checked={this.props.chosenVerbSubordinatingSuffix === 'The fact that...'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="theProspectOfRadio" name="subordinatingSuffixRadio" label="The prospect of..."
                            value="The prospect of..." checked={this.props.chosenVerbSubordinatingSuffix === 'The prospect of...'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="basicGerundRadio" name="subordinatingSuffixRadio" label="Basic gerund" value="Basic gerund"
                            checked={this.props.chosenVerbSubordinatingSuffix === 'Basic gerund'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="presentParticipleRadio" name="subordinatingSuffixRadio" label="Present participle"
                            value="Present participle" checked={this.props.chosenVerbSubordinatingSuffix === 'Present participle'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="wayOfDoingRadio" name="subordinatingSuffixRadio" label="Way of..." value="Way of..."
                            checked={this.props.chosenVerbSubordinatingSuffix === 'Way of...'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="beforeBeingRadio" name="subordinatingSuffixRadio" label="Before being..."
                            value="Before being..." checked={this.props.chosenVerbSubordinatingSuffix === 'Before being...'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="whenDoingRadio" name="subordinatingSuffixRadio" label="When ...ing"
                            value="When ...ing" checked={this.props.chosenVerbSubordinatingSuffix === 'When ...ing'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="sinceDoingRadio" name="subordinatingSuffixRadio" label="Since ...ing"
                            value="Since ...ing" checked={this.props.chosenVerbSubordinatingSuffix === 'Since ...ing'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="byTheTimeRadio" name="subordinatingSuffixRadio" label="By the time..."
                            value="By the time..." checked={this.props.chosenVerbSubordinatingSuffix === 'By the time...'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="asIf2Radio" name="subordinatingSuffixRadio" label="As if ...ing"
                            value="As if ...ing" checked={this.props.chosenVerbSubordinatingSuffix === 'As if ...ing'}
                            onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                        <CustomInput type="radio" id="noTAM42Radio" name="subordinatingSuffixRadio" label="None" value=""
                            checked={this.props.chosenVerbSubordinatingSuffix === ''} onChange={this.handleSubordinatingSuffixChoice}></CustomInput>
                    </div>
                </FormGroup>
            </Form>;

        let possessionMarkerContent = 
        <Form>
            <FormGroup>
                <Label for="radios"><small>Click to add the possessor of the action of the verb:</small></Label>
                <div>
                    <CustomInput type="radio" id="myRadio" name="possessionMarkerRadio" label="my" value="my"
                        checked={this.props.chosenVerbPossessionMarker === 'my'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="yoursRadio" name="possessionMarkerRadio" label="your (s.)" value="your (s.)"
                        checked={this.props.chosenVerbPossessionMarker === 'your (s.)'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="hisheritsRadio" name="possessionMarkerRadio" label="his/her/its" value="his/her/its"
                        checked={this.props.chosenVerbPossessionMarker === 'his/her/its'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="ourRadio" name="possessionMarkerRadio" label="our" value="our"
                        checked={this.props.chosenVerbPossessionMarker === 'our'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="yourpRadio" name="possessionMarkerRadio" label="your (p.)" value="your (p.)"
                        checked={this.props.chosenVerbPossessionMarker === 'your (p.)'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="theirRadio" name="possessionMarkerRadio" label="their" value="their"
                        checked={this.props.chosenVerbPossessionMarker === 'their'} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="noTAM42Radio" name="possessionMarkerRadio" label="None" value=""
                        checked={this.props.chosenVerbPossessionMarker === ''} onChange={this.handlePossessionMarkerChoice}></CustomInput>
                </div>
            </FormGroup>
        </Form>;

        let caseMarkerContent = 
        <Form>
            <FormGroup>
                <Label for="radios"><small>Click to add the case of the verbal noun:</small></Label>
                <div>
                    <CustomInput type="radio" id="nominativeRadio" name="caseMarkerRadio" label="Nominative" value="Nominative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Nominative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="accusativeRadio" name="caseMarkerRadio" label="Accusative" value="Accusative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Accusative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="dativeRadio" name="caseMarkerRadio" label="Dative" value="Dative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Dative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="genitiveRadio" name="caseMarkerRadio" label="Genitive" value="Genitive"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Genitive'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="locativeRadio" name="caseMarkerRadio" label="Locative" value="Locative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Locative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="ablativeRadio" name="caseMarkerRadio" label="Ablative" value="Ablative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Ablative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="comitativeRadio" name="caseMarkerRadio" label="Comitative" value="Comitative"
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === 'Comitative'} onChange={this.handleCaseMarkerChoice}></CustomInput>
                    <CustomInput type="radio" id="noTAM42Radio" name="caseMarkerRadio" label="None" value=""
                        disabled={this.props.chosenVerbSubordinatingSuffix === ''} checked={this.props.chosenVerbCaseMarker === ''} onChange={this.handleCaseMarkerChoice}></CustomInput>
                </div>
            </FormGroup>
        </Form>;

        let verbElementCards =
            <div className="d-flex flex-row justify-content-center w-100 p-1">
                <VerbElementCard header="Voice" content={voiceContent} advisory={this.props.chosenVerbTurkishPassiveAvailable ? 'This verb has the full range of voices available' : 'This verb can only take the Active and Causative voices'}></VerbElementCard>
                <VerbElementCard header="Negative" content={negativeContent}></VerbElementCard>
                    <VerbElementCard header="Finiteness" content={finitenessContent}></VerbElementCard>
                { ((this.props.chosenVerbFiniteness !== 'Finite') || this.props.chosenVerbNegative) && (this.props.chosenVerbVoice !== 'Imperative') ? null : <VerbElementCard header="TAM Block 2" content={tam2Content}></VerbElementCard> }
                { (this.props.chosenVerbFiniteness !== 'Finite') && (this.props.chosenVerbVoice !== 'Imperative') ? null :
                    <VerbElementCard header="TAM Block 3" content={tam3Content}></VerbElementCard> }
                { (this.props.chosenVerbFiniteness !== 'Finite') && (this.props.chosenVerbVoice !== 'Imperative') ? null :
                    <VerbElementCard header="TAM Block 4" content={tam4Content}></VerbElementCard> }
                { (this.props.chosenVerbFiniteness !== 'Finite') && (this.props.chosenVerbVoice !== 'Imperative') ? null :
                    <VerbElementCard header="Person" content={personContent}></VerbElementCard> }
                { (this.props.chosenVerbFiniteness !== 'Subordinate') && (this.props.chosenVerbVoice !== 'Imperative') ? null :
                    <VerbElementCard header="Subordinating Suffixes" content={subordinatingSuffixContent}></VerbElementCard> }
                { (this.props.chosenVerbFiniteness === 'Subordinate') && ((this.props.chosenVerbSubordinatingSuffix === 'As if ...ing') || (this.props.chosenVerbSubordinatingSuffix === 'The fact that...') || (this.props.chosenVerbSubordinatingSuffix === 'The prospect of...')  || (this.props.chosenVerbSubordinatingSuffix === 'Basic gerund') || (this.props.chosenVerbSubordinatingSuffix === 'Way of...')) && (this.props.chosenVerbVoice !== 'Imperative') ?
                    <VerbElementCard header="Possession Markers" content={possessionMarkerContent}></VerbElementCard> : null }
                { (this.props.chosenVerbFiniteness === 'Subordinate') && (this.props.chosenVerbSubordinatingSuffix !== 'As if ...ing') && (this.props.chosenVerbSubordinatingSuffix !== 'Before being...') && (this.props.chosenVerbSubordinatingSuffix !== 'When ...ing') && (this.props.chosenVerbSubordinatingSuffix !== 'Since ...ing') && (this.props.chosenVerbSubordinatingSuffix !== 'By the time...') && (this.props.chosenVerbVoice !== 'Imperative') ?
                    <VerbElementCard header="Case Markers" content={caseMarkerContent}></VerbElementCard> : null }
            </div>

        let verbElementAlerts =
            <div className="d-flex flex-row justify-content-center w-100 p-1">
                { this.props.chosenVerbStem === '' ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbStem} element="Stem"></VerbElementAlert> }
                { this.props.chosenVerbVoice === '' ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbVoiceParticle} element="Voice" elementType={this.props.chosenVerbVoice}>
                    </VerbElementAlert> }
                { !this.props.chosenVerbExtraCausative ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbExtraCausativeParticle} element="Extra Causative"></VerbElementAlert> }
                { !this.props.chosenVerbInability ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbInabilityParticle} element="Inability"></VerbElementAlert> }
                { !this.props.chosenVerbInability && !this.props.chosenVerbNegative ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbNegativeParticle} element="Negative"></VerbElementAlert> }
                { (this.props.chosenVerbFiniteness !== 'Finite') || (this.props.chosenVerbTAM2 === '') || this.props.chosenVerbNegative ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbTAM2Particle} element="TAM2" elementType={this.props.chosenVerbTAM2}></VerbElementAlert> }
                { (this.props.chosenVerbFiniteness !== 'Finite') || (this.props.chosenVerbTAM3 === '') ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbTAM3Particle} element="TAM3" elementType={this.props.chosenVerbTAM3}></VerbElementAlert> }
                { (this.props.chosenVerbFiniteness !== 'Finite') || (this.props.chosenVerbTAM4 === '') ? null : 
                    <VerbElementAlert turkish={this.props.chosenVerbTAM4Particle} element="TAM4" elementType={this.props.chosenVerbTAM4.replace(/_/g, " ")}></VerbElementAlert> }
                { (this.props.chosenVerbFiniteness === 'Finite') && (this.props.chosenVerbPerson != '') ?  
                    <VerbElementAlert turkish={this.props.chosenVerbPersonParticle} element="Person" elementType={this.props.chosenVerbPerson}></VerbElementAlert> : null }
                { (this.props.chosenVerbFiniteness == 'Finite') && this.props.chosenVerbModality ?  
                    <VerbElementAlert turkish={this.props.chosenVerbModalityParticle} element="Modality"></VerbElementAlert> : null }
                { (this.props.chosenVerbFiniteness == 'Subordinate') && (this.props.chosenVerbSubordinatingSuffix !== "") ?    
                    <VerbElementAlert turkish={this.props.chosenVerbSubordinatingSuffixParticle} element="Subordinating Suffix" elementType={this.props.chosenVerbSubordinatingSuffix}></VerbElementAlert> : null }
                { ((this.props.chosenVerbFiniteness == 'Subordinate') && ((this.props.chosenVerbSubordinatingSuffix == 'The fact that...') || (this.props.chosenVerbSubordinatingSuffix == 'The prospect of...') || (this.props.chosenVerbSubordinatingSuffix == 'Basic gerund') || (this.props.chosenVerbSubordinatingSuffix == 'Way of...')) && (this.props.chosenVerbPossessionMarker !== "")) ? 
                    <VerbElementAlert turkish={this.props.chosenVerbPossessionMarkerParticle} element="Possession Marker" elementType={this.props.chosenVerbPossessionMarker}></VerbElementAlert> : null }
                { ((this.props.chosenVerbFiniteness == 'Subordinate') && (((this.props.chosenVerbSubordinatingSuffix == 'The fact that') || (this.props.chosenVerbSubordinatingSuffix == 'The prospect of...') || (this.props.chosenVerbSubordinatingSuffix == 'Basic gerund') || (this.props.chosenVerbSubordinatingSuffix == 'Way of...'))) && (this.props.chosenVerbPossessionMarker !== "") && (this.props.chosenVerbCaseMarker !== "") || ((this.props.chosenVerbSubordinatingSuffix == 'Present participle') && (this.props.chosenVerbCaseMarker !== ""))) ?  
                    <VerbElementAlert turkish={this.props.chosenVerbCaseMarkerParticle} element="Case Marker" elementType={this.props.chosenVerbCaseMarker}></VerbElementAlert> : null }
                
            </div>

        let finiteParticles = 
            this.props.chosenVerbTAM2Particle + this.props.chosenVerbTAM3Particle + this.props.chosenVerbTAM4Particle + this.props.chosenVerbPersonParticle + this.props.chosenVerbModalityParticle;

        let subordinateParticles = 
            this.props.chosenVerbSubordinatingSuffixParticle + this.props.chosenVerbPossessionMarkerParticle + this.props.chosenVerbCaseMarkerParticle;

        let postNegativeParticles = this.props.chosenVerbFiniteness === "" ? "" : 
            (this.props.chosenVerbFiniteness === "Finite" ? finiteParticles : subordinateParticles);

        let concatenatedTurkish = 
            this.props.chosenVerbStem + this.props.chosenVerbVoiceParticle + this.props.chosenVerbExtraCausativeParticle + this.props.chosenVerbInabilityParticle + this.props.chosenVerbNegativeParticle + postNegativeParticles; 
        
        return(
            <div className={classes.VerbEngine}>
                <Container fluid>
                    <Row className={classes.VerbEngineHeader}>
                        <Col>
                            <h1>Turkish Verbs</h1>        
                        </Col>
                    </Row>
                    <Row className={classes.VerbEngineContent}>
                        <div className="d-flex flex-column w-100">
                            <div className="d-flex justify-content-center w-100">
                                <div className={classes.ChooseVerbPrompt}>{this.props.chosenVerbTurkish === '' ? 'Use the dropdown list on the left to choose a verb and build it up into what you want to say.' : ''}</div>
                            </div>
                            <div className="d-flex flex-md-row flex-column w-100">
                                <div className="d-flex w-25 justify-content-center">
                                    <ButtonDropdown isOpen={this.state.verbDropdownOpen} toggle={this.verbDropdownToggle}>
                                        <DropdownToggle caret color="warning">Choose verb</DropdownToggle>
                                        <DropdownMenu className={classes.VerbListDropdownMenu}>
                                            {this.state.verbDropdownItems}
                                        </DropdownMenu>
                                    </ButtonDropdown>
                                </div>
                                <div className="d-flex w-75 justify-content-center align-items-center">
                                    <b>{this.props.chosenVerbTurkish === '' ? null : this.props.chosenVerbTurkish}</b> {this.props.chosenVerbTurkish === '' ? null : ' - ' + this.props.chosenVerbEnglish} 
                                </div>
                            </div>
                            <div className="d-flex w-100">
                                {this.props.chosenVerbTurkish === '' ? null : verbElementCards}
                            </div>
                            <div className="d-flex w-100">
                                {this.props.chosenVerbTurkish === '' ? null : verbElementAlerts}
                            </div>
                            <div className="w-100 text-center">
                                <p><b>{concatenatedTurkish}</b></p>
                            </div>
                            {/* {concatenatedTurkish === '' ? null :
                                <div className="w-100 text-center">
                                    <Button onClick={() => this.translate()} color="warning"><b>Translate</b></Button>
                                </div> }
                            {this.state.translation === '' ? null : 
                                <div className="w-100 text-center">{this.state.translation}</div> } */}
                        </div>
                    </Row>
                </Container>
            </div>
        );

    }

}

const mapStateToProps = state => {
    return {
        chosenVerbTurkish: state.reducer.chosenVerbTurkish,
        chosenVerbStem: state.reducer.chosenVerbStem,
        chosenVerbEnglish: state.reducer.chosenVerbEnglish,
        chosenVerbEnglishGerund: state.reducer.chosenVerbEnglishGerund,
        chosenVerbEnglishThirdPersonSingularPresent: state.reducer.chosenVerbEnglishThirdPersonSingularPresent,
        chosenVerbEnglishThirdPersonPluralPresent: state.reducer.chosenVerbEnglishThirdPersonPluralPresent,
        chosenVerbEnglishPast: state.reducer.chosenVerbEnglishPast,
        chosenVerbEnglishThirdPersonPluralPast: state.reducer.chosenVerbEnglishThirdPersonPluralPast,
        chosenVerbEnglishPastParticiple: state.reducer.chosenVerbEnglishPastParticiple,
        chosenVerbTurkishPassiveAvailable: state.reducer.chosenVerbTurkishPassiveAvailable,
        chosenVerbVoice: state.reducer.chosenVerbVoice,
        chosenVerbExtraCausative: state.reducer.chosenVerbExtraCausative,
        chosenVerbInability: state.reducer.chosenVerbInability,
        chosenVerbNegative: state.reducer.chosenVerbNegative,
        chosenVerbFiniteness: state.reducer.chosenVerbFiniteness,
        chosenVerbTAM2: state.reducer.chosenVerbTAM2,
        chosenVerbTAM3: state.reducer.chosenVerbTAM3,
        chosenVerbTAM4: state.reducer.chosenVerbTAM4,
        chosenVerbPerson: state.reducer.chosenVerbPerson,
        chosenVerbModality: state.reducer.chosenVerbModality,
        chosenVerbSubordinatingSuffix: state.reducer.chosenVerbSubordinatingSuffix,
        chosenVerbPossessionMarker: state.reducer.chosenVerbPossessionMarker,
        chosenVerbCaseMarker: state.reducer.chosenVerbCaseMarker,
        chosenVerbVoiceParticle: state.reducer.chosenVerbVoiceParticle,
        chosenVerbExtraCausativeParticle: state.reducer.chosenVerbExtraCausativeParticle,
        chosenVerbInabilityParticle: state.reducer.chosenVerbInabilityParticle,
        chosenVerbNegativeParticle: state.reducer.chosenVerbNegativeParticle,
        chosenVerbTAM2Particle: state.reducer.chosenVerbTAM2Particle,
        chosenVerbTAM3Particle: state.reducer.chosenVerbTAM3Particle,
        chosenVerbTAM4Particle: state.reducer.chosenVerbTAM4Particle,
        chosenVerbPersonParticle: state.reducer.chosenVerbPersonParticle,
        chosenVerbModalityParticle: state.reducer.chosenVerbModalityParticle,
        chosenVerbSubordinatingSuffixParticle: state.reducer.chosenVerbSubordinatingSuffixParticle,
        chosenVerbPossessionMarkerParticle: state.reducer.chosenVerbPossessionMarkerParticle,
        chosenVerbCaseMarkerParticle: state.reducer.chosenVerbCaseMarkerParticle
    }
};

const mapDispatchToProps = dispatch => ({
    setChosenVerbTurkish: chosenChosenVerbTurkish =>
        dispatch(setChosenVerbTurkish(chosenChosenVerbTurkish)),
    setChosenVerbStem: chosenChosenVerbStem =>
        dispatch(setChosenVerbStem(chosenChosenVerbStem)),
    setChosenVerbEnglish: chosenVerbEnglish =>
        dispatch(setChosenVerbEnglish(chosenVerbEnglish)),
    setChosenVerbEnglishGerund: chosenVerbEnglishGerund =>
        dispatch(setChosenVerbEnglishGerund(chosenVerbEnglishGerund)),
    setChosenVerbEnglishThirdPersonSingularPresent: chosenVerbEnglishThirdPersonSingularPresent =>
        dispatch(setChosenVerbEnglishThirdPersonSingularPresent(chosenVerbEnglishThirdPersonSingularPresent)),
    setChosenVerbEnglishThirdPersonPluralPresent: chosenVerbEnglishThirdPersonPluralPresent =>
        dispatch(setChosenVerbEnglishThirdPersonPluralPresent(chosenVerbEnglishThirdPersonPluralPresent)),
    setChosenVerbEnglishPast: chosenVerbEnglishPast =>
        dispatch(setChosenVerbEnglishPast(chosenVerbEnglishPast)),
    setChosenVerbEnglishThirdPersonPluralPast: chosenVerbEnglishThirdPersonPluralPast =>
        dispatch(setChosenVerbEnglishThirdPersonPluralPast(chosenVerbEnglishThirdPersonPluralPast)),
    setChosenVerbEnglishPastParticiple: chosenVerbEnglishPastParticiple =>
        dispatch(setChosenVerbEnglishPastParticiple(chosenVerbEnglishPastParticiple)),
    setChosenVerbTurkishPassiveAvailable: chosenVerbTurkishPassiveAvailable =>
        dispatch(setChosenVerbTurkishPassiveAvailable(chosenVerbTurkishPassiveAvailable)),
    setChosenVerbVoice: chosenVerbVoice =>
        dispatch(setChosenVerbVoice(chosenVerbVoice)),
    setChosenVerbExtraCausative: chosenVerbExtraCausative =>
        dispatch(setChosenVerbExtraCausative(chosenVerbExtraCausative)),
    setChosenVerbInability: chosenVerbInability =>
        dispatch(setChosenVerbInability(chosenVerbInability)),
    setChosenVerbNegative: chosenVerbNegative =>
        dispatch(setChosenVerbNegative(chosenVerbNegative)),
    setChosenVerbFiniteness: chosenVerbFiniteness =>
        dispatch(setChosenVerbFiniteness(chosenVerbFiniteness)),
    setChosenVerbTAM2: chosenVerbTAM2 =>
        dispatch(setChosenVerbTAM2(chosenVerbTAM2)),
    setChosenVerbTAM3: chosenVerbTAM3 =>
        dispatch(setChosenVerbTAM3(chosenVerbTAM3)),
    setChosenVerbTAM4: chosenVerbTAM4 =>
        dispatch(setChosenVerbTAM4(chosenVerbTAM4)),
    setChosenVerbPerson: chosenVerbPerson =>
        dispatch(setChosenVerbPerson(chosenVerbPerson)),
    setChosenVerbModality: chosenVerbModality =>
        dispatch(setChosenVerbModality(chosenVerbModality)),
    setChosenVerbSubordinatingSuffix: chosenVerbSubordinatingSuffix =>
        dispatch(setChosenVerbSubordinatingSuffix(chosenVerbSubordinatingSuffix)),
    setChosenVerbPossessionMarker: chosenVerbPossessionMarker =>
        dispatch(setChosenVerbPossessionMarker(chosenVerbPossessionMarker)),
    setChosenVerbCaseMarker: chosenVerbCaseMarker =>
        dispatch(setChosenVerbCaseMarker(chosenVerbCaseMarker)),
    setChosenVerbVoiceParticle: chosenVerbVoiceParticle =>
        dispatch(setChosenVerbVoiceParticle(chosenVerbVoiceParticle)),
    setChosenVerbExtraCausativeParticle: chosenVerbExtraCausativeParticle =>
        dispatch(setChosenVerbExtraCausativeParticle(chosenVerbExtraCausativeParticle)),
    setChosenVerbInabilityParticle: chosenVerbInabilityParticle =>
        dispatch(setChosenVerbInabilityParticle(chosenVerbInabilityParticle)),
    setChosenVerbNegativeParticle: chosenVerbNegativeParticle =>
        dispatch(setChosenVerbNegativeParticle(chosenVerbNegativeParticle)),
    setChosenVerbTAM2Particle: chosenVerbTAM2Particle =>
        dispatch(setChosenVerbTAM2Particle(chosenVerbTAM2Particle)),
    setChosenVerbTAM3Particle: chosenVerbTAM3Particle =>
        dispatch(setChosenVerbTAM3Particle(chosenVerbTAM3Particle)),
    setChosenVerbTAM4Particle: chosenVerbTAM4Particle =>
        dispatch(setChosenVerbTAM4Particle(chosenVerbTAM4Particle)),
    setChosenVerbPersonParticle: chosenVerbPersonParticle =>
        dispatch(setChosenVerbPersonParticle(chosenVerbPersonParticle)),
    setChosenVerbModalityParticle: chosenVerbModalityParticle =>
        dispatch(setChosenVerbModalityParticle(chosenVerbModalityParticle)),
    setChosenVerbSubordinatingSuffixParticle: chosenVerbSubordinatingSuffixParticle =>
        dispatch(setChosenVerbSubordinatingSuffixParticle(chosenVerbSubordinatingSuffixParticle)),
    setChosenVerbPossessionMarkerParticle: chosenVerbPossessionMarkerParticle =>
        dispatch(setChosenVerbPossessionMarkerParticle(chosenVerbPossessionMarkerParticle)),
    setChosenVerbCaseMarkerParticle: chosenVerbCaseMarkerParticle =>
        dispatch(setChosenVerbCaseMarkerParticle(chosenVerbCaseMarkerParticle))
});

export default connect(mapStateToProps, mapDispatchToProps)(VerbEngine);