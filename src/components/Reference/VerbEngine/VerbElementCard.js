import React from 'react';
import {
    Card,
    CardHeader,
    // CardTitle,
    // CardText,
    CardFooter,
    CardBody
} from 'reactstrap';
import classes from './VerbElementCard.module.css';

const verbElementCard = (props) => {
    return (
        <Card className={classes.VerbElementCard}>
            <CardHeader tag="h6" className={classes.CardHeader}><b>{props.header}</b></CardHeader>
                <CardBody>
                    <div>{props.content}</div>
                </CardBody>
            <CardFooter className="text-muted"><small>{props.advisory}</small></CardFooter>
        </Card> 
    )
}

export default verbElementCard;

