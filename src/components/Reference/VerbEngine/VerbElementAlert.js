import React from 'react';
import { Alert } from 'reactstrap';

const verbElementAlert = (props) => {
    return (
         <Alert color="danger" className="d-flex flex-column justify-content-center m-1">
            { props.turkish === '' ? null :
                <div className="w-100 text-center">
                    <b>{props.turkish}</b>
                </div> }
            <div className="w-100 text-center">
                {props.element}{props.elementType ? ': ' + props.elementType : null }
            </div>
         </Alert>
    )
}

export default verbElementAlert;

