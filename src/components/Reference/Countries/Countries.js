import React from 'react';
import { Container, Row, Col, Alert } from 'reactstrap';
import classes from './Countries.module.css';
import albania from '../../../assets/images/al.png';
import algeria from '../../../assets/images/dz.png';
import armenia from '../../../assets/images/am.png';
import austria from '../../../assets/images/at.png';
import australia from '../../../assets/images/au.png';
import switzerland from '../../../assets/images/ch.png';
import cyprus from '../../../assets/images/cy.png';
import germany from '../../../assets/images/de.png';
import egypt from '../../../assets/images/eg.png';
import uk from '../../../assets/images/gb.png';
import greece from '../../../assets/images/gr.png';
import hungary from '../../../assets/images/hu.png';
import india from '../../../assets/images/in.png';
import iraq from '../../../assets/images/iq.png';
import iran from '../../../assets/images/ir.png';
import jordan from '../../../assets/images/jo.png';
import lebanon from '../../../assets/images/lb.png';
import lithuania from '../../../assets/images/lt.png';
import latvia from '../../../assets/images/lv.png';
import morocco from '../../../assets/images/ma.png';
import northerncyprus from '../../../assets/images/northerncyprus.png';
import oman from '../../../assets/images/om.png';
import portugal from '../../../assets/images/pt.png';
import scotland from '../../../assets/images/scotland.png';
import sweden from '../../../assets/images/se.png';
import tunisia from '../../../assets/images/tn.png';
import turkey from '../../../assets/images/tr.png';
import usa from '../../../assets/images/us.png';
import wales from '../../../assets/images/wales.png';

const countries = () => {

    return (
        <div className={classes.Countries}>
            <Container fluid>
                <Row className={classes.CountriesHeader}>
                    <Col>
                        <h1>Countries in Turkish</h1>        
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={classes.CountriesMessage}>
                            Most countries of the world have Turkish names which aren't all that different to what they are in other languages, including English. However, there are some key exceptions. Note the difference in the initial letter between Iraq and Iran, and also be careful not to get Sweden and Switzerland mixed up. We've also added Turkey itself for good measure, but you may have seen how that's spelled in Turkish before: -
                        </div>        
                    </Col>
                </Row>
                <Row>
                    <Col className={classes.CountriesContent}>
                        <div className="d-flex flex-row flex-wrap justify-content-center">
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={albania}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Arnavutluk</b></div>
                                        <div>Albania</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={germany}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Almanya</b></div>
                                        <div>Germany</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={usa}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Amerika Birleşik Devletleri</b></div>
                                        <div>United States of America</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={australia}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Avustralya</b></div>
                                        <div>Australia</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={austria}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Avusturya</b></div>
                                        <div>Austria</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={uk}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Birleşik Krallık</b></div>
                                        <div>United Kingdom</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={algeria}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Cezayir</b></div>
                                        <div>Algeria</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={armenia}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Ermenistan</b></div>
                                        <div>Armenia</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={morocco}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Fas</b></div>
                                        <div>Morocco</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={wales}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Galler</b></div>
                                        <div>Wales</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={india}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Hindistan</b></div>
                                        <div>India</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={iraq}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Irak</b></div>
                                        <div>Iraq</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={iran}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>İran</b></div>
                                        <div>Iran</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={scotland}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>İskoçya</b></div>
                                        <div>Scotland</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={sweden}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>İsveç</b></div>
                                        <div>Sweden</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={switzerland}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>İsviçre</b></div>
                                        <div>Switzerland</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={cyprus}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Kıbrıs Cumhuriyeti</b></div>
                                        <div>Republic of Cyprus</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={northerncyprus}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Kuzey Kıbrıs</b></div>
                                        <div>Northern Cyprus</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={latvia}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Letonya</b></div>
                                        <div>Latvia</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={lithuania}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Litvanya</b></div>
                                        <div>Lithuania</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={lebanon}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Lübnan</b></div>
                                        <div>Lebanon</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={hungary}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Macaristan</b></div>
                                        <div>Hungary</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={egypt}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Mısır</b></div>
                                        <div>Egypt</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={portugal}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Portekiz</b></div>
                                        <div>Portugal</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={tunisia}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Tunus</b></div>
                                        <div>Tunisia</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={turkey}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Türkiye</b></div>
                                        <div>Turkey</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={oman}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Umman</b></div>
                                        <div>Oman</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={jordan}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Ürdün</b></div>
                                        <div>Jordan</div>
                                    </div>
                                </div>
                            </Alert>
                            <Alert color="danger" className="p-0 m-1">
                                <div className="d-flex flex-row">
                                    <div className="p-1"><img className={classes.Flag} src={greece}></img></div>
                                    <div className="d-flex flex-column p-1">
                                        <div><b>Yunanistan</b></div>
                                        <div>Greece</div>
                                    </div>
                                </div>
                            </Alert>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )

}

export default countries;