import React, { Component } from 'react';
import { Container, Row, Col, Alert, Button } from 'reactstrap';
import TimePicker from 'react-time-picker';
import classes from './Time.module.css';

class Time extends Component {

    constructor(props) {
        super(props);

        this.state = {
            time: '00:00',
            timeTurkish: '',
            minuteStrings: [
                { number: '00', string: ''}, { number: '01', string: 'bir', locative: 'de'}, { number: '02', string: 'iki', locative: 'de'},
                { number: '03', string: 'üç', locative: 'te'}, { number: '04', string: 'dört', locative: 'te'},
                { number: '05', string: 'beş', locative: 'te'}, { number: '06', string: 'altı', locative: 'da'},
                { number: '07', string: 'yedi', locative: 'de'}, { number: '08', string: 'sekiz', locative: 'de'},
                { number: '09', string: 'dokuz', locative: 'da'}, { number: '10', string: 'on', locative: 'da'},
                { number: '11', string: 'on bir', locative: 'de'}, { number: '12', string: 'on iki', locative: 'de'},
                { number: '13', string: 'on üç', locative: 'te'}, { number: '14', string: 'on dört', locative: 'te'},
                { number: '15', string: 'on beş', locative: 'te', special: 'çeyrek', specialLoc: 'te'}, { number: '16', string: 'on altı', locative: 'da'},
                { number: '17', string: 'on yedi', locative: 'de'}, { number: '18', string: 'on sekiz', locative: 'de'},
                { number: '19', string: 'on dokuz', locative: 'da'}, { number: '20', string: 'yirmi', locative: 'de'},
                { number: '21', string: 'yirmi bir', locative: 'de'}, { number: '22', string: 'yirmi iki', locative: 'de'},
                { number: '23', string: 'yirmi üç', locative: 'te'}, { number: '24', string: 'yirmi dört', locative: 'te'},
                { number: '25', string: 'yirmi beş', locative: 'te'}, { number: '26', string: 'yirmi altı', locative: 'da'},
                { number: '27', string: 'yirmi yedi', locative: 'de'}, { number: '28', string: 'yirmi sekiz', locative: 'de'},
                { number: '29', string: 'yirmi dokuz', locative: 'da'}, { number: '30', string: 'otuz', locative: 'da', special: 'buçukta'},
                { number: '31', string: 'otuz bir', locative: 'de'}, { number: '32', string: 'otuz iki', locative: 'de'},
                { number: '33', string: 'otuz üç', locative: 'te'}, { number: '34', string: 'otuz dört', locative: 'te'},
                { number: '35', string: 'otuz beş', locative: 'te'}, { number: '36', string: 'otuz altı', locative: 'da'},
                { number: '37', string: 'otuz yedi', locative: 'de'}, { number: '38', string: 'otuz sekiz', locative: 'de'},
                { number: '39', string: 'otuz dokuz', locative: 'da'}, { number: '40', string: 'kırk', locative: 'ta'},
                { number: '41', string: 'kırk bir', locative: 'de'}, { number: '42', string: 'kırk iki', locative: 'de'},
                { number: '43', string: 'kırk üç', locative: 'te'}, { number: '44', string: 'kırk dört', locative: 'te'},
                { number: '45', string: 'kırk beş', locative: 'te'}, { number: '46', string: 'kırk altı', locative: 'da'},
                { number: '47', string: 'kırk yedi', locative: 'de'}, { number: '48', string: 'kırk sekiz', locative: 'de'},
                { number: '49', string: 'kırk dokuz', locative: 'de'}, { number: '50', string: 'elli', locative: 'de'},
                { number: '51', string: 'elli bir', locative: 'de'}, { number: '52', string: 'elli iki', locative: 'de'},
                { number: '53', string: 'elli üç', locative: 'te'}, { number: '54', string: 'elli dört', locative: 'te'},
                { number: '55', string: 'elli beş', locative: 'te'}, { number: '56', string: 'elli altı', locative: 'da'},
                { number: '57', string: 'elli yedi', locative: 'de'}, { number: '58', string: 'elli sekiz', locative: 'de'},
                { number: '59', string: 'elli dokuz', locative: 'da'}
            ],
            hourStrings: [
                { number: '00', string: 'sıfır', locative: 'da', accusative: 'sıfırı', dative: 'sıfıra', special: 'gece yarısı', specialAcc: 'gece yarısını', specialDat: 'gece yarısına' },
                { number: '01', string: 'bir', locative: 'de', accusative: 'biri', dative: 'bire' },
                { number: '02', string: 'iki', locative: 'de', accusative: 'ikiyi', dative: 'ikiye' },
                { number: '03', string: 'üç', locative: 'te', accusative: 'ücü', dative: 'üce' },
                { number: '04', string: 'dört', locative: 'te', accusative: 'dördü', dative: 'dörde' },
                { number: '05', string: 'beş', locative: 'te', accusative: 'beşi', dative: 'beşe' },
                { number: '06', string: 'altı', locative: 'da', accusative: 'altıyı', dative: 'altıya' },
                { number: '07', string: 'yedi', locative: 'de', accusative: 'yediyi', dative: 'yediye' },
                { number: '08', string: 'sekiz', locative: 'de', accusative: 'sekizi', dative: 'sekize' },
                { number: '09', string: 'dokuz', locative: 'da', accusative: 'dokuzu', dative: 'dokuza' },
                { number: '10', string: 'on', locative: 'da', accusative: 'onu', dative: 'ona' },
                { number: '11', string: 'on bir', locative: 'de', accusative: 'on biri', dative: 'on bire' },
                { number: '12', string: 'on iki', locative: 'de', accusative: 'on ikiyi', dative: 'on ikiye', special: 'öğle', specialAcc: 'öğleyi', specialDat: 'öğleye' },
                { number: '13', string: 'on üç', locative: 'te', accusative: 'on ücü', dative: 'on üce' },
                { number: '14', string: 'on dört', locative: 'te', accusative: 'on dördü', dative: 'e' },
                { number: '15', string: 'on beş', locative: 'te', accusative: 'on beşi', dative: 'on dörde' },
                { number: '16', string: 'on altı', locative: 'da', accusative: 'on altıyı', dative: 'on altıya' },
                { number: '17', string: 'on yedi', locative: 'de', accusative: 'on yediyi', dative: 'on yediye' },
                { number: '18', string: 'on sekiz', locative: 'de', accusative: 'on sekizi', dative: 'on sekize' },
                { number: '19', string: 'on dokuz', locative: 'da', accusative: 'on dokuzu', dative: 'on dokuza' },
                { number: '20', string: 'yirmi', locative: 'de', accusative: 'yirmiyi', dative: 'yirmiye' },
                { number: '21', string: 'yirmi bir', locative: 'de', accusative: 'yirmi biri', dative: 'yirmi bire' },
                { number: '22', string: 'yirmi iki', locative: 'de', accusative: 'yirmi ikiyi', dative: 'yirmi ikiye' },
                { number: '23', string: 'yirmi üç', locative: 'te', accusative: 'yirmi ücü', dative: 'yirmi üce' }
            ],
            timeAlerts: []
        }
    }

    onTimeChange = time => this.setState({time: time});

    getTimeInTurkish() {
        let _timeAlerts;
        let hour = +this.state.time.split(':')[0];
        let minutes = +this.state.time.split(':')[1];
        let hourString12Nom = hour < 13 ? this.state.hourStrings[hour].string : this.state.hourStrings[hour - 12].string;
        let hourString12Loc = hour < 13 ? this.state.hourStrings[hour].locative : this.state.hourStrings[hour - 12].locative;
        let hourString12Acc = hour < 13 ? this.state.hourStrings[hour].accusative : this.state.hourStrings[hour - 12].accusative;
        let hourString12Dat = hour < 13 ? this.state.hourStrings[hour + 1].dative : this.state.hourStrings[hour - 11].dative;
        let hourString12 = minutes < 31 ? hourString12Acc : hourString12Dat;
        let hourString24 = this.state.hourStrings[hour].string;
        let specialHour = (hour === 0) || (hour === 12) ? this.state.hourStrings[hour].special : null;
        let minuteQuarterString = 'çeyrek';
        let halfHourString = 'buçukta';
        let minuteStringNom = 
            minutes < 31 ? this.state.minuteStrings[minutes].string + ' geçe' : this.state.minuteStrings[60 - minutes].string + ' kala';
        let amPM = hour < 12 ? 'sabah' : 'akşam';
        let minuteStringLoc = this.state.minuteStrings[minutes].string + this.state.minuteStrings[minutes].locative;
        let saat = 'saat ';
        let oClock = saat + hourString12Nom + hourString12Loc;
        let timeString12 = '';
        let timeStringSpecialHour = specialHour;
        let timeStringMinutesQuarter = '';
        let timeString24 = saat + hourString24 + ' ' + (minutes > 0 ? minuteStringLoc : '');
        if(minutes === 0) {
            timeString12 = oClock;
        } else {
            timeString12 = saat + hourString12 + ' ' + minuteStringNom + ' '+ amPM;
            if(minutes === 15) {
                timeStringMinutesQuarter = saat + hourString12 + ' ' + minuteQuarterString + ' geçe ' + amPM;
            }
            if(minutes === 30) {
                timeStringMinutesQuarter = saat + hourString12Nom + ' ' + halfHourString + ' ' + amPM;
            }
            if(minutes === 45) {
                timeStringMinutesQuarter = saat + hourString12 + ' ' + minuteQuarterString + ' kala ' + amPM;
            }
        }
        switch(this.state.time) {
            case '00:00':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat on iki sabah</b></div>
                            <div>12AM</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat {timeStringSpecialHour}</b></div>
                            <div>midnight</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat sıfır sıfırda</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '12:00':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour}PM</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringSpecialHour}</b></div>
                            <div>midday/noon</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '01:15':
            case '02:15':
            case '03:15':
            case '04:15':
            case '05:15':
            case '06:15':
            case '07:15':
            case '08:15':
            case '09:15':
            case '10:15':
            case '11:15':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter past {hour} in the morning</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '12:15':
            case '13:15':
            case '14:15':
            case '15:15':
            case '16:15':
            case '17:15':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}:{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter past {hour} in the afternoon</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '18:15':
            case '19:15':
            case '20:15':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter past {hour} in the evening</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '21:15':
            case '22:15':
            case '23:15':
            _timeAlerts = 
            <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter past {hour < 13 ? hour : hour - 12} at night</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '00:15':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat on ikiyi on beş geçe sabah</b></div>
                            <div>12:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat on ikiyi çeyrek geçe sabah</b></div>
                            <div>quarter past 12 at night</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '01:30':
            case '02:30':
            case '03:30':
            case '04:30':
            case '05:30':
            case '06:30':
            case '07:30':
            case '08:30':
            case '09:30':
            case '10:30':
            case '11:30':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>half past {hour} in the morning</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '12:30':
            case '13:30':
            case '14:30':
            case '15:30':
            case '16:30':
            case '17:30':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>half past {hour} in the afternoon</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '18:30':
            case '19:30':
            case '20:30':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>half past {hour} in the evening</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '21:30':
            case '22:30':
            case '23:30':
            _timeAlerts = 
            <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>half past {hour} at night</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '00:30':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat on ikiyi otuz geçe sabah</b></div>
                            <div>12:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat on iki buçukta sabah</b></div>
                            <div>half past 12 at night</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '00:45':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>12:45AM</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>saat bire çeyrek kala sabah</b></div>
                            <div>quarter to 1 in the morning</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '01:45':
            case '02:45':
            case '03:45':
            case '04:45':
            case '05:45':
            case '06:45':
            case '07:45':
            case '08:45':
            case '09:45':
            case '10:45':
            case '11:45':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter to {hour < 13 ? hour + 1 : hour - 11} in the morning</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '12:45':
            case '13:45':
            case '14:45':
            case '15:45':
            case '16:45':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter to {hour < 13 ? hour + 1 : hour - 11} in the afternoon</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '17:45':
            case '18:45':
            case '19:45':
            case '20:45':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter to {hour < 13 ? hour + 1 : hour - 11} in the evening</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '21:45':
            case '22:45':
            case '23:45':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12}</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:{this.state.time.split(':')[1]} {hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeStringMinutesQuarter}</b></div>
                            <div>quarter to {hour < 13 ? hour + 1 : hour - 11} at night</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '01:00':
            case '02:00':
            case '03:00':
            case '04:00':
            case '05:00':
            case '06:00':
            case '07:00':
            case '08:00':
            case '09:00':
            case '10:00':
            case '11:00':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12} sabah</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:00{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12} sabah</b></div>
                            <div>{hour} o'clock in the morning</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '13:00':
            case '14:00':
            case '15:00':
            case '16:00':
            case '17:00':
                _timeAlerts = 
                    <div className="d-flex flex-row align-items-center">
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12} akşam</b></div>
                            <div>{hour < 13 ? hour : hour - 12}:00{hour < 12 ? 'AM' : 'PM'}</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString12} akşam</b></div>
                            <div>{hour} o'clock in the aftenoon</div>
                        </Alert>
                        <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                            <div><b>{timeString24}</b></div>
                            <div>{this.state.time}hrs</div>
                        </Alert>
                    </div>;
                break;
            case '18:00':
            case '19:00':
            case '20:00':
                _timeAlerts = 
                <div className="d-flex flex-row align-items-center">
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString12} akşam</b></div>
                        <div>{hour < 13 ? hour : hour - 12}:00{hour < 12 ? 'AM' : 'PM'}</div>
                    </Alert>
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString12} akşam</b></div>
                        <div>{hour} o'clock in the evening</div>
                    </Alert>
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString24}</b></div>
                        <div>{this.state.time}hrs</div>
                    </Alert>
                </div>;
                break;
            case '21:00':
            case '22:00':
            case '23:00':
                _timeAlerts = 
                <div className="d-flex flex-row align-items-center">
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString12} akşam</b></div>
                        <div>{hour < 13 ? hour : hour - 12}:00{hour < 12 ? 'AM' : 'PM'}</div>
                    </Alert>
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString12} akşam</b></div>
                        <div>{hour} o'clock at night</div>
                    </Alert>
                    <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                        <div><b>{timeString24}</b></div>
                        <div>{this.state.time}hrs</div>
                    </Alert>
                </div>;
                break;
            default:
                if(hour === 0) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour + 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? '12' : '1'} at night</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                } else if(hour > 0 && hour < 12) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? hour : hour + 1} in the morning</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                } else if(hour === 12) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? hour : hour - 11} in the afternoon</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                } else if(hour > 12 && hour < 18) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? hour - 12 : hour - 11} in the afternoon</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                } else if(hour >= 18 && hour <= 20) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? hour - 12 : hour - 11} in the evening</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                } else if(hour > 20 && hour <= 23) {
                    _timeAlerts = 
                        <div className="d-flex flex-row align-items-center">
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString12}</b></div>
                                <div>{hour - 12}:{this.state.time.split(':')[1]}{hour < 12 ? 'AM' : 'PM'}</div>
                                <div>{minutes < 31 ? minutes : 60 - minutes} {minutes < 31 ? 'minute(s) past' : 'minute(s) to'} {minutes < 31 ? hour - 12 : hour - 11} at night</div>
                            </Alert>
                            <Alert className="m-1 p-1 d-flex flex-column align-items-center" color="danger">
                                <div><b>{timeString24}</b></div>
                                <div>{this.state.time}hrs</div>
                            </Alert>
                        </div>;
                }
                break;
        }
        this.setState({ timeAlerts: _timeAlerts })
    }

    render() {

        return(
            <div className={classes.Time}>
                <Container fluid>
                    <Row className={classes.TimeHeader}>
                        <Col>
                            <h1>Time and Time in Turkish</h1>        
                        </Col>
                    </Row>
                    <Row className={classes.TimeContent}>
                        <Col>
                            <Row>
                                <Col>
                                    <Row>
                                        <div>
                                            <p>Telling the time in Turkish is no big headache: -</p>
                                            <p>- <b>o'clock</b> is expresed by using <b><i>saat</i></b> (hour) plus the number of the hour in the locative.</p>
                                            <p>- <b>half past</b> the hour is expressed by using <b><i>saat</i></b> plus the number of the hour in the nominative, and then <b><i>buçukta</i></b>.</p>
                                            <p>- <b>X past</b> the hour, where X is the number of minutes, is expressed by using <b><i>saat</i></b> plus the number of the hour in the accusative, the number of minutes in the nominative, and then <b><i>geçe</i></b>.</p>
                                            <p>- <b>X to</b> the hour, where X is the number of minutes, is expressed by using <b><i>saat</i></b> plus the number of the hour in the dative, the number of minutes in the nominative, and then <b><i>kala</i></b>.</p>
                                            <p>In both of the cases above, if you want to express <b>quarter past/to</b>, you replace the number of minutes with <b><i>çeyrek</i></b>.</p>
                                            <p><b>A.M.</b> is expressed by <b><i>sabah</i></b> or, more formally, <b><i>öğleden önce</i></b> (lit. "before noon"). <b>P.M.</b> is expressed by <b><i>akşam</i></b> or, more formally, <b><i>öğleden sonra</i></b> (lit. "after noon").</p>
                                            <p>In the 24-hour clock, you simply put the number of the hour in the nominative, and the number of minutes in the locative.</p>
                                            <p>They aren't used in telling clock time in Turkish, but the words for <b>minute</b> and <b>second</b> are, respectively, <b><i>dakika</i></b> and <b><i>an</i></b>. Remember that these stay in the singular even when used in a plural sense with a number or other counting word.</p>
                                            <p>Type in a time below to get the time in Turkish. Any special expressions for the hour will also be displayed: -</p>
                                            <p>In all circumstances, the word <b><i>saat</i></b> can be taken to mean "The time is..."</p>
                                            <p>So, let's ask the question, <b><i>Ne kadar saat?</i></b> (lit. "How much is the hour?"): -</p> 
                                        </div>        
                                    </Row>
                                    <Row>
                                        <div className="d-flex flex-row w-100">
                                            <div className="w-25 d-flex flex-row align-items-center">
                                                <div className="m-1">
                                                    <TimePicker onChange={this.onTimeChange} value={this.state.time} className={classes.TimePicker} disableClock={true}/>
                                                </div>
                                                <div className="m-1">
                                                    <Button color="warning" onClick={() => this.getTimeInTurkish()}><b>Get time</b></Button>
                                                </div>
                                            </div>
                                            <div className="w-75 align-items-center">
                                                {this.state.timeAlerts}
                                            </div>
                                        </div>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        );

    }

}

export default Time;