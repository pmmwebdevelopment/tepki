import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Alert 
    // Alert, 
    // Button
} from 'reactstrap';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import classes from './Dates.module.css';

class Dates extends Component {

    constructor(props) {
        super(props);

        this.handleDayClick = this.handleDayClick.bind(this);

        this.state = {
            date: '00:00',
            dateTurkish: '',
            selectedDay: undefined,
            dayStrings: [
                { en: 'Monday', enshort: 'Mon', tr: 'Pazartesi' }, { en: 'Tuesday', enshort: 'Tue', tr: 'Salı' },
                { en: 'Wednesday', enshort: 'Wed', tr: 'Çarsamba' }, { en: 'Thursday', enshort: 'Thu', tr: 'Persembe' },
                { en: 'Friday', enshort: 'Fri', tr: 'Cuma' }, { en: 'Saturday', enshort: 'Sat', tr: 'Cumartesi' },
                { en: 'Sunday', enshort: 'Sun', tr: 'Pazar' } 
            ],
            dateStrings: [
                { number: '1', string: 'bir' }, { number: '2', string: 'iki' }, { number: '3', string: 'üç' }, { number: '4', string: 'dört' },
                { number: '5', string: 'beş' }, { number: '6', string: 'altı' }, { number: '7', string: 'yedi' }, { number: '8', string: 'sekiz' },
                { number: '9', string: 'dokuz' }, { number: '10', string: 'on' }, { number: '11', string: 'on bir' }, 
                { number: '12', string: 'on iki' }, { number: '13', string: 'on üç' }, { number: '14', string: 'on dört' },
                { number: '15', string: 'on beş' }, { number: '16', string: 'on altı' }, { number: '17', string: 'on yedi' },
                { number: '18', string: 'on sekiz' }, { number: '19', string: 'on dokuz' }, { number: '20', string: 'yirmi' },
                { number: '21', string: 'yirmi bir' }, { number: '22', string: 'yirmi iki' }, { number: '23', string: 'yirmi üç' },
                { number: '24', string: 'yirmi dört' }, { number: '25', string: 'yirmi beş' }, { number: '26', string: 'yirmi altı' },
                { number: '27', string: 'yirmi yedi' }, { number: '28', string: 'yirmi sekiz' }, { number: '29', string: 'yirmi dokuz' },
                { number: '30', string: 'otuz' }, { number: '31', string: 'otuz bir' }
            ],
            monthStrings: [
                { en: 'January', enshort: 'Jan', tr: 'Ocak', trLoc: 'Ocakta', trPosLoc: 'Ocağında' },
                { en: 'February', enshort: 'Feb', tr: 'Şubat', trLoc: 'Şubatta', trPosLoc: 'Şubadında' },
                { en: 'March', enshort: 'Mar', tr: 'Mart', trLoc: 'Martta', trPosLoc: 'Mardında' },
                { en: 'April', enshort: 'Apr', tr: 'Nisan', trLoc: 'Nisanda', trPosLoc: 'Nisanında' },
                { en: 'May', enshort: 'May', tr: 'Mayıs', trLoc: 'Mayısta', trPosLoc: 'Mayısında' },
                { en: 'June', enshort: 'Jun', tr: 'Haziran', trLoc: 'Haziranda', trPosLoc: 'Haziranında' },
                { en: 'July', enshort: 'Jul', tr: 'Temmuz', trLoc: 'Temmuzda', trPosLoc: 'Temmuzunda' },
                { en: 'August', enshort: 'Aug', tr: 'Ağustos', trLoc: 'Ağustosta', trPosLoc: 'Ağustosunda' },
                { en: 'September', enshort: 'Sep', tr: 'Eylül', trLoc: 'Eylülde', trPosLoc: 'Eylülünde' },
                { en: 'October', enshort: 'Oct', tr: 'Ekim', trLoc: 'Ekimde', trPosLoc: 'Ekiminde' },
                { en: 'November', enshort: 'Nov', tr: 'Kasım', trLoc: 'Kasımda', trPosLoc: 'Kasımında' },
                { en: 'December', enshort: 'Dec', tr: 'Aralık', trLoc: 'Aralıkta', trPosLoc: 'Aralığında' },
            ],
            seasonStrings: [
                { en: 'Spring', tr: 'Bahar', trIn: 'Baharda'}, { en: 'Summer', tr: 'Yaz', trIn: 'Yazın'},
                { en: 'Autumn', tr: 'Sonbahar', trIn: 'Sonbaharda'}, { en: 'Winter', tr: 'Kış', trIn: 'Kışın'}, 
            ],
            digitStrings: [ '', 'bir', 'iki', 'üç', 'dört', 'beş', 'altı', 'yedi', 'sekiz', 'dokuz' ],
            dateAlert: ''
        }
    }

    capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    handleDayClick(day) {
        this.setState({ selectedDay: day.toString() }, () => {
                let dayOfWeek = this.state.selectedDay.split(' ')[0];
                let month = this.state.selectedDay.split(' ')[1];
                let dateInMonth = +this.state.selectedDay.split(' ')[2] - 1;
                let year = this.state.selectedDay.split(' ')[3];
                let trDayOfWeek = '';
                for(let _dayOfWeek = 0; _dayOfWeek < this.state.dayStrings.length; _dayOfWeek += 1) {
                    if(dayOfWeek === this.state.dayStrings[_dayOfWeek].enshort) {
                        trDayOfWeek = this.state.dayStrings[_dayOfWeek].tr
                    } else { continue; }
                }
                let trDateInMonth = this.state.dateStrings[dateInMonth].string;
                let trMonthNom = '';
                let trMonthPosLoc = '';
                for(let _month = 0; _month < this.state.monthStrings.length; _month += 1) {
                    if(month === this.state.monthStrings[_month].enshort) {
                        trMonthNom = this.state.monthStrings[_month].tr;
                        trMonthPosLoc = this.state.monthStrings[_month].trPosLoc;
                    } else { continue; }
                }
                let yilNom = 'yıl'; 
                let yilGen = 'yılın';
                let yearUnits = ' ' + this.state.digitStrings[+(year[year.length - 1])]; 
                let yearTens = '';
                if (year[year.length - 2] === '1') {
                    yearTens = ' on';
                } else {
                    ' ' + this.state.digitStrings[+(year[year.length - 2])] + ' on';
                }
                let yearHundreds = '';
                if (year[year.length - 3] === '1') {
                    yearTens = ' yüz';
                } else {
                    ' ' + this.state.digitStrings[+(year[year.length - 3])] + ' yüz';
                }
                let yearThousands = year[year.length - 4] === '1' ? ' bin' : ' ' + this.state.digitStrings[+(year[year.length - 4])] + ' bin';
                let trYearString =
                    +year < 1000 ? yearHundreds + yearTens + yearUnits : yearThousands +  yearHundreds + yearTens + yearUnits;
                let trDateFormat1 = trDayOfWeek + ', ' + trDateInMonth + ' ' + trMonthNom + ', ' + trYearString + ' ' + yilNom;
                let trDateFormat2 =
                    trDayOfWeek + ',' + trYearString + ' ' + yilGen + ' ' + trDateInMonth + ' ' + trMonthPosLoc;
                let _dateAlert =
                    <Alert className="p-1 m-1" color="danger" key={day}>
                        <div className="d-flex flex-column align-items-center">
                            <div><b>{trDateFormat1}</b></div>
                            <div><b>{trDateFormat2}</b></div>
                            <div>{dayOfWeek} {dateInMonth} {month} {year}</div>
                        </div>
                    </Alert>;
                this.setState({ dateAlert: _dateAlert });
            }
        );
    }

    render() {

        let daysOfTheWeekAlerts = [];
        for(let day = 0; day < this.state.dayStrings.length; day += 1) {
            let dayAlert =
                <Alert className="p-1 m-1" color="danger" key={day}>
                    <div className="d-flex flex-column align-items-center">
                        <div><b>{this.state.dayStrings[day].tr}</b></div>
                        <div>{this.state.dayStrings[day].en}</div>
                    </div>
                </Alert>;
            daysOfTheWeekAlerts.push(dayAlert);
        }

        let monthAlerts = [];
        for(let month = 0; month < this.state.monthStrings.length; month += 1) {
            let monthAlert = 
                <Alert className="p-1 m-1" color="danger" key={month}>
                    <div className="d-flex flex-column align-items-center">
                        <div><b>{this.state.monthStrings[month].tr}</b></div>
                        <div>{this.state.monthStrings[month].en}</div>
                    </div>
                </Alert>;
            monthAlerts.push(monthAlert);
        }

        let monthLocativeAlerts = [];
        for(let monthLoc = 0; monthLoc < this.state.monthStrings.length; monthLoc += 1) {
            let monthLocAlert = 
                <Alert className="p-1 m-1" color="danger" key={monthLoc}>
                    <div className="d-flex flex-column align-items-center">
                        <div><b>{this.state.monthStrings[monthLoc].trLoc}</b></div>
                        <div>in {this.state.monthStrings[monthLoc].en}</div>
                    </div>
                </Alert>;
            monthLocativeAlerts.push(monthLocAlert);
        }

        let seasonAlerts = [];
        for(let season = 0; season < this.state.seasonStrings.length; season += 1) {
            let seasonAlert = 
                <Alert className="p-1 m-1" color="danger" key={season}>
                    <div className="d-flex flex-column align-items-center">
                        <div><b>{this.state.seasonStrings[season].tr}</b></div>
                        <div>{this.state.seasonStrings[season].en}</div>
                    </div>
                </Alert>;
            seasonAlerts.push(seasonAlert);
        }

        for(let season = 0; season < this.state.seasonStrings.length; season += 1) {
            let seasonAlert = 
                <Alert className="p-1 m-1" color="danger" key={season + 4}>
                    <div className="d-flex flex-column align-items-center">
                        <div><b>{this.state.seasonStrings[season].trIn}</b></div>
                        <div>in {this.state.seasonStrings[season].en}</div>
                    </div>
                </Alert>;
            seasonAlerts.push(seasonAlert);
        }

        return(
            <div className={classes.Dates}>
                <Container fluid>
                    <Row className={classes.DatesHeader}>
                        <Col>
                            <h1>Dates in Turkish</h1>        
                        </Col>
                    </Row>
                    <Row className={classes.DatesContent}>
                        <Col>
                            <Row>
                                <Col>
                                    <Row>
                                        <div>
                                            <p>Quoting dates in Turkish is fairly straightforward, but there are a number of options: -</p>
                                            <p>We'll start with the smallest unit first, days of the week: -</p>
                                        </div>        
                                    </Row>
                                    <Row>
                                        <div className="d-flex flex-row justify-content-center w-100">
                                            {daysOfTheWeekAlerts}
                                        </div>
                                    </Row>
                                    <Row>
                                        <div>
                                            <p>The word for <b>day</b> in Turkish is <b><i>gün</i></b>.</p>
                                            <p>Like English, but unlike many other languages using Latin script, all units of time from days upwards in length begin with a capital letter in Turkish. To say <b>"on X-day"</b>, you simply use the basic, nominative form.</p>
                                            <p>Turkey has operated officially on the Gregorian calendar since the foundation of the Republic in 1923, but the months of the year in Turkish reflect the country's Islamic heritage, with many of the names adopted from the Muslim, lunar calendar: -</p>
                                        </div>
                                    </Row>
                                    <Row>
                                        <div className="d-flex flex-row justify-content-center w-100">
                                            {monthAlerts}
                                        </div>
                                    </Row>
                                    <Row>
                                        <div>
                                            <p>To say "in X month", you put the month in the locative: -</p>
                                        </div>
                                    </Row>
                                    <Row>
                                        <div className="d-flex flex-row justify-content-center w-100">
                                            {monthLocativeAlerts}
                                        </div>
                                    </Row>
                                    <Row>
                                        <div>
                                            <p>The word for <b>month</b> in Turkish is <b><i>ay</i></b>.</p>
                                            <p>The word for <b>season</b> in Turkish is <b><i>mevsim</i>. </b>The seasons of the year in Turkish are as follows: -</p>
                                        </div>
                                    </Row>
                                    <Row>
                                        <div className="d-flex flex-row justify-content-center w-100">
                                            {seasonAlerts}
                                        </div>
                                    </Row>
                                    <Row>
                                        <div>
                                            <p>You'll notice that, to say "in X season", Spring and Autumn are in the <b>locative</b>, while Summer and Winter are in the <b>genitive</b>.</p>
                                            <p>The word for year in Turkish is <b><i>yıl</i></b>. To say "in X year", this takes the <b>locative</b> <b><i>yılda</i></b>. We can now construct full dates :-</p>
                                            <p>- Day of the month and month in nominative, year with <b><i>yıl</i></b> in locative, or;</p>
                                            <p>- Year with <b><i>yıl</i></b> in genitive, day of the month in nominative, month with his/her/its possessive marker and locative ending, or;</p>
                                            <p>Pick a date from the calendar below to get the full date in Turkish: -</p>
                                        </div>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <div className="d-flex flex-row">
                                                <div className="d-flex flex-row w-25">
                                                    <div>
                                                        <DayPicker className={classes.DayPicker} onDayClick={this.handleDayClick} selectedDates={this.state.selectedDay}>
                                                        </DayPicker>
                                                    </div>
                                                </div>
                                                <div className="d-flex flex-column w-75 align-items-center justify-content-center">
                                                    {this.state.dateAlert}
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        );

    }

}

export default Dates;