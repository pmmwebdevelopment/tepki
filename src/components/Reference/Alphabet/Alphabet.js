import React, { Component } from 'react';
import axios from 'axios';
import Interweave from 'interweave';
import {
    Container,
    Row,
    Col,
    ButtonGroup,
    Button,
    Alert
} from 'reactstrap';
import classes from './Alphabet.module.css';

class Alphabet extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            letters: [],
            chosenLetterNumber: null,
            letterCommentary: ''
        };

    }

    componentDidMount() {
        this.getAlphabet();
    }

    getAlphabet() {
        this.setState({loading: true});
        axios.get('https://tepki-pmm2.firebaseio.com/letters.json')
        .then(response => {
            this.setState({
                    loading: false,
                    letters: response.data,
            });
        }).catch(error => {
            console.log('There has been an error');
            this.setState({loading: false});
        });
    }

    handleLetterChoice(_index) {
        this.setState({loading: true});
        axios.get('https://pastebin.com/raw/' + this.state.letters[_index].url)
        .then(response => {
            this.setState({chosenLetterNumber: _index, letterCommentary: response.data, loading: false});
        })
        .catch(error => console.log('There has been an error'));
    }

    render () {

        let letterButtons = this.state.letters.map((letter, index) =>
            <Button color="warning" key={index} onClick={() => this.handleLetterChoice(index)} active={this.state.chosenLetterNumber === index}><b>{letter.upper}</b></Button> 
        );

        let exampleAlerts = [];

        if(this.state.chosenLetterNumber !== null) {
            exampleAlerts = this.state.letters[this.state.chosenLetterNumber].examples.map((example, index) =>
                <Alert key={index} color="danger" className="d-flex flex-column p-2 m-2 text-center">
                    <div><b>{example.tr}</b></div>
                    <div>{example.en}</div>
                </Alert>
            );
        }

        
        return (
            <div className={classes.Alphabet}>
                <Container fluid>
                    <Row className={classes.AlphabetHeader}>
                        <Col>
                            <h1>The Turkish Alphabet</h1>        
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className={classes.AlphabetMessage}>
                                <ButtonGroup className="m-1">
                                    {letterButtons ? letterButtons : null }
                                </ButtonGroup>
                            </div>        
                        </Col>
                    </Row>
                    <Row>
                        <Col md="3" className={classes.AlphabetContent}>
                            <div className="d-flex align-items-center justify-content-center h-100">
                                <div>
                                    { this.state.chosenLetterNumber !== null ? <div className={classes.LetterDisplay}><b>{this.state.letters[this.state.chosenLetterNumber].upper} {this.state.letters[this.state.chosenLetterNumber].lower}</b></div> : null }          
                                </div>
                            </div>
                        </Col>
                        <Col md="9" className={classes.AlphabetContent}>
                            <Row>
                                <Interweave tagName="div" content={this.state.letterCommentary}/>
                            </Row>
                            <Row class="w-100 d-flex justify-content-center">
                                {exampleAlerts}
                            </Row>          
                        </Col>
                    </Row>
                </Container>
            </div>
        )

    }

}

export default Alphabet;