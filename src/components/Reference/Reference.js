import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';
import classes from './Reference.module.css';

const reference = () => {

    return (
        <div className={classes.Reference}>
            <Container fluid>
                <Row className={classes.ReferenceHeader}>
                    <Col>
                        <h1>Reference Section</h1>        
                    </Col>
                </Row>
                <Row className={classes.ReferenceSubHeader}>                    
                    <Col>
                        <div className={classes.ReferenceMessage}>
                            <p>Here, you will find some useful guides to help understand parts of the Turkish language.</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={classes.ReferenceContent}>
                            <Button color="danger" size="lg" block>
                                <Link to='/alphabet' style={{ textDecoration: 'none', color: 'white' }} >
                                    The Turkish Alphabet
                                </Link>
                            </Button>
                            <Button color="danger" size="lg" block>
                                <Link to='/time' style={{ textDecoration: 'none', color: 'white' }} >
                                    Telling the Time
                                </Link>
                            </Button>
                            <Button color="danger" size="lg" block>
                                <Link to='/dates' style={{ textDecoration: 'none', color: 'white' }} >
                                    Dates
                                </Link>
                            </Button>
                            <Button color="danger" size="lg" block>
                                <Link to='/countries' style={{ textDecoration: 'none', color: 'white' }} >
                                    Countries
                                </Link>
                            </Button>
                            <Button color="danger" size="lg" block>
                                <Link to='/verbengine' style={{ textDecoration: 'none', color: 'white' }} >
                                    Verbs
                                </Link>
                            </Button>
                            <Button disabled color="danger" size="lg" block>
                                {/* <Link to='/links' style={{ textDecoration: 'none', color: 'white' }} > */}
                                    Useful links (coming soon)
                                {/* </Link> */}
                            </Button>
                        </div>        
                    </Col>
                </Row>
            </Container>
        </div>
    )   
}

export default reference;