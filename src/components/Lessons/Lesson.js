import React, { Component } from 'react';
// import firebase from "../../../higherorder/firebase/firebase"; 
import axios from 'axios';
import Interweave from 'interweave';
// import { Link } from 'react-router-dom';
import { Container, Row, Col, Button, Alert } from 'reactstrap';
import classes from './Lesson.module.css';
import { connect } from 'react-redux';
import {
    setLessonNumber,
    setLessonName,
    setLessonCommentaryID,
    setLessonLayout,
    setUsersBookmarkID,
    commitBookmarkToCentralStore
} from '../../store/actions';
import Spinner from '../UI/Spinner';
import Highlighter from "react-highlight-words";

class Lesson extends Component {

    constructor(props) {
        super(props);

        this.state = {
            lessonCommentary: '',
            lessonExamples: [],
            lessonsWithNoExamples: [1, 2, 3, 5, 6, 7, 16, 17, 28, 38, 39, 44, 52, 56, 57, 59, 60],
            loading: false,
        }
    }

    componentDidMount() {
        this.getLessonHeaders(this.props.chosenLessonNumber);
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps.chosenLessonNumber !== this.props.chosenLessonNumber) {
            this.getLessonHeaders(nextProps.chosenLessonNumber);
        }
    }

    getLesson(chosenLessonNumber, chosenLessonURL) {
        this.setState({loading: true});
        axios.get('https://pastebin.com/raw/' + chosenLessonURL)
        .then(response => {
            this.setState({lessonCommentary: response.data, loading: false});
            if (this.state.lessonsWithNoExamples.indexOf(+chosenLessonNumber) === -1) {
                axios.get('https://tepki-pmm2.firebaseio.com/sentences.json?orderBy="lesson"&equalTo="' + chosenLessonNumber + '"')
                    .then(response => {
                        this.setState({ lessonExamples: response.data });    
                    } 
                )
                .catch(error => console.log('There has been an error'));
            } else {
                this.setState({ lessonExamples: {} });
            }
        })
        .catch(error => console.log('There has been an error'));

    }

    getLessonHeaders(chosenLessonNumber) {
        axios.get('https://tepki-pmm2.firebaseio.com/lessons/' + (+chosenLessonNumber - 1) + '.json')
        .then(response => {
                this.props.setLessonNumber(response.data.no);
                this.props.setLessonName(response.data.title);
                this.props.setLessonCommentaryID(response.data.url);
                this.props.setLessonLayout(response.data.layout);
                this.getLesson(this.props.chosenLessonNumber, this.props.chosenLessonCommentaryID, );
            })
            .catch(error => console.log('There has been an error'));
    }

    handleLessonChoice(chosenLessonNumber) {
        this.props.setLessonNumber(chosenLessonNumber.toString());
    }
    
    setLayout(layout) {
        const displayExamples = [];
        if(Object.keys(this.state.lessonExamples).length > 0) {
            
            for (
                    let i = +Object.keys(this.state.lessonExamples)[0];
                    i < (+Object.keys(this.state.lessonExamples)[0] + Object.keys(this.state.lessonExamples).length);
                    i += 1
            ) {
    
                const Highlight = ({ children }) => (
                    <span className={classes.ExampleHighlighted}>{children}</span>
                );
    
                displayExamples.push(
                    <Alert className={classes.LessonExample} color="danger" key={this.state.lessonExamples[i][0]}>
                        <b><Highlighter highlightTag={Highlight} searchWords={[this.state.lessonExamples[i].operative]} autoEscape={true} textToHighlight={this.state.lessonExamples[i].tr}/></b><br/>
                        {this.state.lessonExamples[i].en}
                    </Alert>
                );
            }    
        }

        switch(layout) {
            case 'p':
                return (
                    <Row className={classes.LessonContent}>
                        <Col md='6'>
                            <div className={classes.LessonCommentary}>
                            <Interweave tagName="div" content={this.state.lessonCommentary}/>
                            </div>
                        </Col>
                        <Col md='6'>
                            <div className={classes.LessonExamples}>
                                {displayExamples}
                            </div>
                        </Col>
                    </Row>
                );
            case 'l':
                return (
                    <Row className={classes.LessonContent}>
                        <Col md='12'>
                            <div className={classes.LessonCommentary}>
                            <Interweave tagName="div" content={this.state.lessonCommentary}/>
                            </div>
                        </Col>
                        <Col md='12'>
                            <div className={classes.LessonExamples}>
                                {displayExamples}
                            </div>
                        </Col>
                    </Row>
                );
            case 'n':
                return (
                    <Row className={classes.LessonContent}>
                        <Col md='12'>
                            <div className={classes.LessonCommentary}>
                            <Interweave tagName="div" content={this.state.lessonCommentary}/>
                            </div>
                        </Col>
                    </Row>
                );
            default:
                return (
                    <Row className={classes.LessonContent}>
                        <Col md='12'>
                            <div className={classes.LessonCommentary}>
                            <Interweave tagName="div" content={this.state.lessonCommentary}/>
                            </div>
                        </Col>
                    </Row>
                );
        }
    }

    // saveBookmark(bookmark, bookmarkId) {
    //     firebase.auth().currentUser.getIdToken().then((idToken) => {
    //         if (this.props.bookmarkedLessonNumber !== null) {
    //             this.props.commitBookmarkToCentralStore(bookmark.bookmarkNumber);
    //             // if user has bookmark - update it
    //             axios.put('https://tepki-pmm2.firebaseio.com/bookmarks/' + bookmarkId + '.json?auth=' + idToken, bookmark);
    //         } else {
    //             this.props.commitBookmarkToCentralStore(bookmark.bookmarkNumber);
    //             // if user has no bookmark - create one    
    //             axios.post('https://tepki-pmm2.firebaseio.com/bookmarks.json?auth=' + idToken, bookmark);
    //         }
    //     },
    //         (err) => {
    //           window.alert(err.message);
    //     });
    // }

    render() {

        let bookmarkButtonText = this.props.chosenLesson === this.props.bookmarkedLessonNumber ? 'Lesson bookmarked' : 'Bookmark this lesson';

        let bookmarkButtonDisabled = this.props.chosenLesson === this.props.bookmarkedLessonNumber;

        let bookmarkData = {
            user: this.props.userEmail,
            bookmarkNumber: this.props.chosenLesson
          }

        return (
            <div className={classes.Lesson}>
                <Container fluid>
                    <Row className={classes.LessonHeader}>
                        <Col>
                            <h1>{this.props.chosenLessonNumber} - {this.props.chosenLessonName}</h1>        
                        </Col>
                    </Row>
                    {!this.state.loading ? this.setLayout(this.props.chosenLessonLayout) : <Spinner/>}
                    <Row>
                        <Col>
                            <Button
                                to='/lesson'
                                className={classes.LessonButton}
                                color='warning'
                                size='md'
                                disabled={this.props.chosenLessonNumber === '1'}
                                onClick={() => {
                                    // this.getNewLessonDetails(this.props.chosenLesson - 1);
                                    this.handleLessonChoice(+this.props.chosenLessonNumber - 1);
                                }}>
                                    Previous lesson
                            </Button>
                            <Button
                                to='/lesson'
                                className={classes.LessonButton}
                                color='warning'
                                size='md'
                                disabled={this.props.chosenLessonNumber === '79'}
                                onClick={() => {
                                    this.handleLessonChoice(+this.props.chosenLessonNumber + 1);
                                }}>
                                    Next lesson
                            </Button>
                            {
                                this.props.authState ? 
                                <Button
                                    to='/lesson'
                                    className={classes.LessonButton}
                                    color='info'
                                    size='md'
                                    disabled={bookmarkButtonDisabled}
                                    onClick={() => this.saveBookmark(bookmarkData, this.props.usersBookmarkID)}
                                >
                                    {bookmarkButtonText}
                                </Button> : null
                            }
                        </Col>
                    </Row>
                </Container>
            </div>
        )   
    }
}

const mapStateToProps = state => {
    return {
        chosenLessonNumber: state.reducer.chosenLessonNumber,
        chosenLessonName: state.reducer.chosenLessonName,
        chosenLessonCommentaryID: state.reducer.chosenLessonCommentaryID,
        chosenLessonLayout: state.reducer.chosenLessonLayout,
        userEmail: state.reducer.userEmail,
        authState: state.reducer.authState,
        usersBookmarkID: state.reducer.usersBookmarkID,
        bookmarkedLessonNumber: state.reducer.bookmarkedLessonNumber,
    }
};

const mapDispatchToProps = dispatch => ({
    setLessonNumber: chosenLessonNumber => dispatch(setLessonNumber(chosenLessonNumber)),
    setLessonName: chosenLessonName => dispatch(setLessonName(chosenLessonName)),
    setLessonCommentaryID: chosenLessonCommentaryID => dispatch(setLessonCommentaryID(chosenLessonCommentaryID)),
    setLessonLayout: chosenLessonLayout => dispatch(setLessonLayout(chosenLessonLayout)),
    setUsersBookmarkID: usersBookmarkID => dispatch(setUsersBookmarkID(usersBookmarkID)),
    commitBookmarkToCentralStore: bookmarkedLessonNumber => dispatch(commitBookmarkToCentralStore(bookmarkedLessonNumber))
});

export default connect (mapStateToProps, mapDispatchToProps) (Lesson);