import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    setLessonNumber,
    setLessonName,
    setLessonCommentaryID,
    setLessonLayout
} from '../../store/actions';

import { Container, Row, Col } from 'reactstrap';
import axios from 'axios';
import classes from './Lessons.module.css';
import GrammarPointButton from './GrammarPointButton';
import Spinner from '../UI/Spinner';


class Lessons extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            grammarPointButtons: []
        }
    }

    componentDidMount() {
        this.getLessons();
    }

    getLessons() {
        this.setState({loading: true});
        axios.get('https://tepki-pmm2.firebaseio.com/lessons.json')
        .then(response => {
            this.setGrammarPointButtons(response.data);
            this.setState({loading: false});
        }).catch(error => {
            console.log('There has been an error');
            this.setState({loading: false});
        });
    }

    setGrammarPointButtons(lessons) {
        const grammarPointButtonList = [];
        for (let i = 0; i < lessons.length; i += 1) {
            let buttonColor = lessons[i].no === this.props.bookmarkedLessonNumber ? 'success' : 'danger';
            grammarPointButtonList.push(
                <GrammarPointButton color={buttonColor} onClick={() => this.handleLessonChoice(lessons[i])} key={i} grammarpoint={lessons[i].title} />
            );
        }
        this.setState({
            grammarPointButtons: grammarPointButtonList
        });
    }

    handleLessonChoice(chosenLesson) {
        this.props.setLessonNumber(chosenLesson.no);
        this.props.setLessonName(chosenLesson.title);
        this.props.setLessonCommentaryID(chosenLesson.url);
        this.props.setLessonLayout(chosenLesson.layout);
    }

    render() {

        let lessonButtonsDisplay =
                <Container fluid>
                    <Row className={classes.LessonsHeader}>
                        <Col>
                            <h1>Lessons</h1>        
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className={classes.LessonsContainer}>
                                {this.state.grammarPointButtons}
                            </div>
                        </Col>
                    </Row>
                </Container>
        
        let loaderSpinner = <Spinner></Spinner>

        let display = this.state.loading ? loaderSpinner : lessonButtonsDisplay

        return (
            <div className={classes.Lessons}>
                {display}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        chosenLessonNumber: state.reducer.chosenLessonNumber,
        chosenLessonName: state.reducer.chosenLessonName,
        chosenLessonCommentaryID: state.reducer.chosenLessonCommentaryID,
        chosenLessonLayout: state.reducer.chosenLessonLayout,
        authState: state.reducer.authState,
        bookmarkedLessonNumber: state.reducer.bookmarkedLessonNumber,
    }
};

const mapDispatchToProps = dispatch => ({
    setLessonNumber: chosenLessonNumber => dispatch(setLessonNumber(chosenLessonNumber)),
    setLessonName: chosenLessonName => dispatch(setLessonName(chosenLessonName)),
    setLessonCommentaryID: chosenLessonCommentaryID => dispatch(setLessonCommentaryID(chosenLessonCommentaryID)),
    setLessonLayout: chosenLessonLayout => dispatch(setLessonLayout(chosenLessonLayout)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Lessons);