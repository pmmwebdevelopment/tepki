import React from 'react';
import classes from './Spinner.module.css';

const spinner = () => (
    <div className={classes.Loader}>Loading… / Yükleyor…</div>
);

export default spinner;