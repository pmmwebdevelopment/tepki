import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';
import classes from './Home.module.css';

const home = () => {
    return (
        <div className={classes.Home}>
            <Container fluid>
                <Row className={classes.HomeHeader}>
                    <Col>
                        <h1>TEPKİ</h1>
                    </Col>
                </Row>
                <Row>
                    <Col className={classes.HomeSubHeader}>
                        <h2>Cool tips for learners of Turkish</h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={classes.HomeMessage}>
                            <h3>Learning Turkish&#63; Ever <i>thought</i> of learning but believed that it was just too difficult&#63; This may just be the site for you. Here, you&apos;ll find tips on writing and grammar which won&apos;t necessarily make you fluent but will unlock some of the secrets of this mysterious and beautiful language.</h3>
                            <h4>Turkish is thought by some to be a Semitic language, related to other languages of the Middle East such as Arabic. Others believe it to be totally unrelated to any other language. In fact, neither is true: Turkish is, in terms of the number of spearkers, the largest of a group of languages spoken between Turkey itself and the western regions of China. Its rich heritage is testament to the development of a civilisation which is still steeped in mystery outside Turkey's own borders.</h4>
                            <h4>Use the links at the top of the page to navigate through a series of lessons which will break down Turkish grammar into bitesize chunks.</h4>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={classes.HomeButton}>
                            <Button color="danger">
                                <Link to='/lessons' style={{ textDecoration: 'none', color: 'white' }}>
                                    Click here to begin the journey
                                </Link>
                            </Button>{' '}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default home;