import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import asyncComponent from './higherorder/asyncComponent/asyncComponent';
import classes from './App.css';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './components/Home/Home';

const asyncLessons = asyncComponent(() => {
  return import('./components/Lessons/Lessons');
});

const asyncLesson = asyncComponent(() => {
  return import('./components/Lessons/Lesson');
});

const asyncReference = asyncComponent(() => {
  return import('./components/Reference/Reference');
});

const asyncCountries = asyncComponent(() => {
  return import('./components/Reference/Countries/Countries');
});

const asyncTime = asyncComponent(() => {
  return import('./components/Reference/DatesAndTimes/Time');
});

const asyncDates = asyncComponent(() => {
  return import('./components/Reference/DatesAndTimes/Dates');
});

const asyncVerbEngine = asyncComponent(() => {
  return import('./components/Reference/VerbEngine/VerbEngine');
});

const asyncAlphabet = asyncComponent(() => {
  return import('./components/Reference/Alphabet/Alphabet');
});

class App extends Component {

  render() {

    let routes = (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/lessons" component={asyncLessons} />
        <Route path="/lesson" component={asyncLesson} />
        <Route path="/reference" component={asyncReference} />
        <Route path="/countries" component={asyncCountries} />
        <Route path="/time" component={asyncTime} />
        <Route path="/dates" component={asyncDates} />
        <Route path="/verbengine" component={asyncVerbEngine} />
        <Route path="/alphabet" component={asyncAlphabet} />
        {/* <Route path="/signuplogin" component={asyncSignupLogin} /> */}
        <Redirect to="/" />
      </Switch>
    );

    return (
      <div className={classes.App}>
        <Header />
          {routes}
          <Footer />
      </div>
    );
  }
}

export default withRouter(App);
