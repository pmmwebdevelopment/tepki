// LESSONS
export const SET_LESSON_NUMBER = 'SET_LESSON_NUMBER';
export const SET_LESSON_NAME = 'SET_LESSON_NAME';
export const SET_LESSON_COMMENTARY_ID = 'SET_LESSON_COMMENTARY_ID';
export const SET_LESSON_LAYOUT = 'SET_LESSON_LAYOUT';
export const setLessonNumber = chosenLessonNumber => { return { type: SET_LESSON_NUMBER, chosenLessonNumber } }
export const setLessonName = chosenLessonName => { return { type: SET_LESSON_NAME, chosenLessonName } }
export const setLessonCommentaryID = chosenLessonCommentaryID => { return { type: SET_LESSON_COMMENTARY_ID, chosenLessonCommentaryID } }
export const setLessonLayout = chosenLessonLayout => { return { type: SET_LESSON_LAYOUT, chosenLessonLayout } }

// VERB ENGINE
export const SET_CHOSEN_VERB_TURKISH = 'SET_CHOSEN_VERB_TURKISH';
export const SET_CHOSEN_VERB_STEM = 'SET_CHOSEN_VERB_STEM';
export const SET_CHOSEN_VERB_ENGLISH = 'SET_CHOSEN_VERB_ENGLISH';
export const SET_CHOSEN_VERB_ENGLISH_GERUND = 'SET_CHOSEN_VERB_ENGLISH_GERUND';
export const SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_SINGULAR_PRESENT = 'SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_SINGULAR_PRESENT';
export const SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PRESENT = 'SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PRESENT';
export const SET_CHOSEN_VERB_ENGLISH_PAST = 'SET_CHOSEN_VERB_ENGLISH_PAST';
export const SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PAST = 'SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PAST';
export const SET_CHOSEN_VERB_ENGLISH_PAST_PARTICIPLE = 'SET_CHOSEN_VERB_ENGLISH_PAST_PARTICIPLE';
export const SET_CHOSEN_VERB_PASSIVE_AVAILABLE = 'SET_CHOSEN_VERB_PASSIVE_AVAILABLE';
export const SET_CHOSEN_VERB_VOICE = 'SET_CHOSEN_VERB_VOICE';
export const SET_CHOSEN_VERB_EXTRA_CAUSATIVE = 'SET_CHOSEN_VERB_EXTRA_CAUSATIVE';
export const SET_CHOSEN_VERB_INABILITY = 'SET_CHOSEN_VERB_INABILITY';
export const SET_CHOSEN_VERB_NEGATIVE = 'SET_CHOSEN_VERB_NEGATIVE';
export const SET_CHOSEN_VERB_FINITENESS = 'SET_CHOSEN_VERB_FINITENESS';
export const SET_CHOSEN_VERB_TAM2 = 'SET_CHOSEN_VERB_TAM2';
export const SET_CHOSEN_VERB_TAM3 = 'SET_CHOSEN_VERB_TAM3';
export const SET_CHOSEN_VERB_TAM4 = 'SET_CHOSEN_VERB_TAM4';
export const SET_CHOSEN_VERB_PERSON = 'SET_CHOSEN_VERB_PERSON';
export const SET_CHOSEN_VERB_MODALITY = 'SET_CHOSEN_VERB_MODALITY';
export const SET_CHOSEN_VERB_SUBORDINATING_SUFFIX = 'SET_CHOSEN_VERB_SUBORDINATING_SUFFIX';
export const SET_CHOSEN_VERB_POSSESSION_MARKER = 'SET_CHOSEN_VERB_POSSESSION_MARKER';
export const SET_CHOSEN_VERB_CASE_MARKER = 'SET_CHOSEN_VERB_CASE_MARKER';
export const SET_CHOSEN_VERB_VOICE_PARTICLE = 'SET_CHOSEN_VERB_VOICE_PARTICLE';
export const SET_CHOSEN_VERB_EXTRA_CAUSATIVE_PARTICLE = 'SET_CHOSEN_VERB_EXTRA_CAUSATIVE_PARTICLE';
export const SET_CHOSEN_VERB_INABILITY_PARTICLE = 'SET_CHOSEN_VERB_INABILITY_PARTICLE';
export const SET_CHOSEN_VERB_NEGATIVE_PARTICLE = 'SET_CHOSEN_VERB_NEGATIVE_PARTICLE';
export const SET_CHOSEN_VERB_TAM2_PARTICLE = 'SET_CHOSEN_VERB_TAM2_PARTICLE';
export const SET_CHOSEN_VERB_TAM3_PARTICLE = 'SET_CHOSEN_VERB_TAM3_PARTICLE';
export const SET_CHOSEN_VERB_TAM4_PARTICLE = 'SET_CHOSEN_VERB_TAM4_PARTICLE';
export const SET_CHOSEN_VERB_PERSON_PARTICLE = 'SET_CHOSEN_VERB_PERSON_PARTICLE';
export const SET_CHOSEN_VERB_MODALITY_PARTICLE = 'SET_CHOSEN_VERB_MODALITY_PARTICLE';
export const SET_CHOSEN_VERB_SUBORDINATING_SUFFIX_PARTICLE = 'SET_CHOSEN_VERB_SUBORDINATING_SUFFIX_PARTICLE';
export const SET_CHOSEN_VERB_POSSESSION_MARKER_PARTICLE = 'SET_CHOSEN_VERB_POSSESSION_MARKER_PARTICLE';
export const SET_CHOSEN_VERB_CASE_MARKER_PARTICLE = 'SET_CHOSEN_VERB_CASE_MARKER_PARTICLE';
export const setChosenVerbTurkish = chosenVerbTurkish =>
{ return { type: SET_CHOSEN_VERB_TURKISH, chosenVerbTurkish } }
export const setChosenVerbStem = chosenVerbStem =>
    { return { type: SET_CHOSEN_VERB_STEM, chosenVerbStem } }
export const setChosenVerbEnglish = chosenVerbEnglish =>
    { return { type: SET_CHOSEN_VERB_ENGLISH, chosenVerbEnglish } }
export const setChosenVerbEnglishGerund = chosenVerbEnglishGerund =>
    { return { type: SET_CHOSEN_VERB_ENGLISH_GERUND, chosenVerbEnglishGerund } }
export const setChosenVerbEnglishThirdPersonSingularPresent = chosenVerbEnglishThirdPersonSingularPresent => 
    { return { type: SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_SINGULAR_PRESENT, chosenVerbEnglishThirdPersonSingularPresent } }
export const setChosenVerbEnglishThirdPersonPluralPresent = chosenVerbEnglishThirdPersonPluralPresent =>
    { return { type: SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PRESENT, chosenVerbEnglishThirdPersonPluralPresent } }
export const setChosenVerbEnglishPast = chosenVerbEnglishPast =>
    { return { type: SET_CHOSEN_VERB_ENGLISH_PAST, chosenVerbEnglishPast } }
export const setChosenVerbEnglishThirdPersonPluralPast = chosenVerbEnglishThirdPersonPluralPast =>
    { return { type: SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PAST, chosenVerbEnglishThirdPersonPluralPast } }
export const setChosenVerbEnglishPastParticiple = chosenVerbEnglishPastParticiple =>
    { return { type: SET_CHOSEN_VERB_ENGLISH_PAST_PARTICIPLE, chosenVerbEnglishPastParticiple } }
export const setChosenVerbTurkishPassiveAvailable = chosenVerbTurkishPassiveAvailable =>
    { return { type: SET_CHOSEN_VERB_PASSIVE_AVAILABLE, chosenVerbTurkishPassiveAvailable } }
export const setChosenVerbVoice = chosenVerbVoice =>
    { return { type: SET_CHOSEN_VERB_VOICE, chosenVerbVoice } }
export const setChosenVerbExtraCausative = chosenVerbExtraCausative =>
    { return { type: SET_CHOSEN_VERB_EXTRA_CAUSATIVE, chosenVerbExtraCausative } }
export const setChosenVerbInability = chosenVerbInability =>
    { return { type: SET_CHOSEN_VERB_INABILITY, chosenVerbInability } }
export const setChosenVerbNegative = chosenVerbNegative =>
    { return { type: SET_CHOSEN_VERB_NEGATIVE, chosenVerbNegative } }
export const setChosenVerbFiniteness = chosenVerbFiniteness =>
    { return { type: SET_CHOSEN_VERB_FINITENESS, chosenVerbFiniteness } }
export const setChosenVerbTAM2 = chosenVerbTAM2 =>
    { return { type: SET_CHOSEN_VERB_TAM2, chosenVerbTAM2 } }
export const setChosenVerbTAM3 = chosenVerbTAM3 =>
    { return { type: SET_CHOSEN_VERB_TAM3, chosenVerbTAM3 } }
export const setChosenVerbTAM4 = chosenVerbTAM4 =>
    { return { type: SET_CHOSEN_VERB_TAM4, chosenVerbTAM4 } }
export const setChosenVerbPerson = chosenVerbPerson =>
    { return { type: SET_CHOSEN_VERB_PERSON, chosenVerbPerson } }
export const setChosenVerbModality = chosenVerbModality =>
    { return { type: SET_CHOSEN_VERB_MODALITY, chosenVerbModality } }
export const setChosenVerbSubordinatingSuffix = chosenVerbSubordinatingSuffix =>
    { return { type: SET_CHOSEN_VERB_SUBORDINATING_SUFFIX, chosenVerbSubordinatingSuffix } }
export const setChosenVerbPossessionMarker = chosenVerbPossessionMarker =>
    { return { type: SET_CHOSEN_VERB_POSSESSION_MARKER, chosenVerbPossessionMarker } }
export const setChosenVerbCaseMarker = chosenVerbCaseMarker =>
    { return { type: SET_CHOSEN_VERB_CASE_MARKER, chosenVerbCaseMarker } }
export const setChosenVerbVoiceParticle = chosenVerbVoiceParticle =>
    { return { type: SET_CHOSEN_VERB_VOICE_PARTICLE, chosenVerbVoiceParticle } }
export const setChosenVerbExtraCausativeParticle = chosenVerbExtraCausativeParticle =>
    { return { type: SET_CHOSEN_VERB_EXTRA_CAUSATIVE_PARTICLE, chosenVerbExtraCausativeParticle } }
export const setChosenVerbInabilityParticle = chosenVerbInabilityParticle =>
    { return { type: SET_CHOSEN_VERB_INABILITY_PARTICLE, chosenVerbInabilityParticle } }
export const setChosenVerbNegativeParticle = chosenVerbNegativeParticle =>
    { return { type: SET_CHOSEN_VERB_NEGATIVE_PARTICLE, chosenVerbNegativeParticle } }
export const setChosenVerbTAM2Particle = chosenVerbTAM2Particle =>
    { return { type: SET_CHOSEN_VERB_TAM2_PARTICLE, chosenVerbTAM2Particle } }
export const setChosenVerbTAM3Particle = chosenVerbTAM3Particle =>
    { return { type: SET_CHOSEN_VERB_TAM3_PARTICLE, chosenVerbTAM3Particle } }
export const setChosenVerbTAM4Particle = chosenVerbTAM4Particle =>
    { return { type: SET_CHOSEN_VERB_TAM4_PARTICLE, chosenVerbTAM4Particle } }
export const setChosenVerbPersonParticle = chosenVerbPersonParticle =>
    { return { type: SET_CHOSEN_VERB_PERSON_PARTICLE, chosenVerbPersonParticle } }
export const setChosenVerbModalityParticle = chosenVerbModalityParticle =>
    { return { type: SET_CHOSEN_VERB_MODALITY_PARTICLE, chosenVerbModalityParticle } }
export const setChosenVerbSubordinatingSuffixParticle = chosenVerbSubordinatingSuffixParticle =>
    { return { type: SET_CHOSEN_VERB_SUBORDINATING_SUFFIX_PARTICLE, chosenVerbSubordinatingSuffixParticle } }
export const setChosenVerbPossessionMarkerParticle = chosenVerbPossessionMarkerParticle =>
    { return { type: SET_CHOSEN_VERB_POSSESSION_MARKER_PARTICLE, chosenVerbPossessionMarkerParticle } }
export const setChosenVerbCaseMarkerParticle = chosenVerbCaseMarkerParticle =>
    { return { type: SET_CHOSEN_VERB_CASE_MARKER_PARTICLE, chosenVerbCaseMarkerParticle } }
    
// USER MANAGEMENT
export const SET_USER_EMAIL = 'SET_USER_EMAIL';
export const SET_AUTH_STATE = 'SET_AUTH_STATE';
export const COMMIT_BOOKMARK_TO_CENTRAL_STORE = 'COMMIT_BOOKMARK_TO_CENTRAL_STORE';
export const SET_USERS_BOOKMARK_ID = 'SET_USERS_BOOKMARK_ID';
export const setAuthState = authState => { return { type: SET_AUTH_STATE, authState } }
export const setUserEmail = userEmail => { return { type: SET_USER_EMAIL, userEmail } }
export const commitBookmarkToCentralStore = bookmarkedLessonNumber =>{ return { type: COMMIT_BOOKMARK_TO_CENTRAL_STORE, bookmarkedLessonNumber } }
export const setUsersBookmarkID = usersBookmarkID => { return { type: SET_USERS_BOOKMARK_ID, usersBookmarkID } }



