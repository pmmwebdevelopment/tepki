import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import tepkiReducers from "./reducers";

const store = createStore(
    tepkiReducers,
    compose(
        applyMiddleware(thunk)
        // applyMiddleware(thunk),
        // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);

export default store;
