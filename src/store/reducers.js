import { combineReducers } from 'redux';
import {
  // LESSONS
    SET_LESSON_NUMBER,
    SET_LESSON_NAME,
    SET_LESSON_COMMENTARY_ID,
    SET_LESSON_LAYOUT,

  // VERB ENGINE
    SET_CHOSEN_VERB_TURKISH,
    SET_CHOSEN_VERB_STEM,
    SET_CHOSEN_VERB_ENGLISH,
    SET_CHOSEN_VERB_ENGLISH_GERUND,
    SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_SINGULAR_PRESENT,
    SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PRESENT,
    SET_CHOSEN_VERB_ENGLISH_PAST,
    SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PAST,
    SET_CHOSEN_VERB_ENGLISH_PAST_PARTICIPLE,
    SET_CHOSEN_VERB_PASSIVE_AVAILABLE,
    SET_CHOSEN_VERB_VOICE,
    SET_CHOSEN_VERB_EXTRA_CAUSATIVE,
    SET_CHOSEN_VERB_INABILITY,
    SET_CHOSEN_VERB_NEGATIVE,
    SET_CHOSEN_VERB_FINITENESS,
    SET_CHOSEN_VERB_TAM2,
    SET_CHOSEN_VERB_TAM3,
    SET_CHOSEN_VERB_TAM4,
    SET_CHOSEN_VERB_PERSON,
    SET_CHOSEN_VERB_MODALITY,
    SET_CHOSEN_VERB_SUBORDINATING_SUFFIX,
    SET_CHOSEN_VERB_POSSESSION_MARKER,
    SET_CHOSEN_VERB_CASE_MARKER,
    SET_CHOSEN_VERB_VOICE_PARTICLE,
    SET_CHOSEN_VERB_EXTRA_CAUSATIVE_PARTICLE,
    SET_CHOSEN_VERB_INABILITY_PARTICLE,
    SET_CHOSEN_VERB_NEGATIVE_PARTICLE,
    SET_CHOSEN_VERB_TAM2_PARTICLE,
    SET_CHOSEN_VERB_TAM3_PARTICLE,
    SET_CHOSEN_VERB_TAM4_PARTICLE,
    SET_CHOSEN_VERB_PERSON_PARTICLE,
    SET_CHOSEN_VERB_MODALITY_PARTICLE,
    SET_CHOSEN_VERB_SUBORDINATING_SUFFIX_PARTICLE,
    SET_CHOSEN_VERB_POSSESSION_MARKER_PARTICLE,
    SET_CHOSEN_VERB_CASE_MARKER_PARTICLE,
    
  // USERS
    SET_USER_EMAIL,
    SET_AUTH_STATE,
    COMMIT_BOOKMARK_TO_CENTRAL_STORE,
    SET_USERS_BOOKMARK_ID
} from './actions';

const initialState = {
  // LESSONS
    chosenLessonNumber: '1',
    chosenLessonName: 'Vowel harmony & stress',
    chosenLessonCommentaryID: 'LessAPnF',
    chosenLessonLayout: 'n',

  // VERB ENGINE
    chosenVerbTurkish: '',
    chosenVerbStem: '',
    chosenVerbEnglish: '',
    chosenVerbEnglishGerund: '',
    chosenVerbEnglishThirdPersonSingularPresent: '',
    chosenVerbEnglishThirdPersonPluralPresent: '',
    chosenVerbEnglishPast: '',
    chosenVerbEnglishThirdPersonPluralPast: '',
    chosenVerbEnglishPastParticiple: '',
    chosenVerbTurkishPassiveAvailable: true,
    chosenVerbVoice: '',
    chosenVerbExtraCausative: false,
    chosenVerbInability: false,
    chosenVerbNegative: false,
    chosenVerbFiniteness: '',
    chosenVerbTAM2: '',
    chosenVerbTAM3: '',
    chosenVerbTAM4: '',
    chosenVerbPerson: '',
    chosenVerbModality: false,
    chosenVerbSubordinatingSuffix: '',
    chosenVerbPossessionMarker: '',
    chosenVerbCaseMarker: '',
    chosenVerbVoiceParticle: '',
    chosenVerbExtraCausativeParticle: '',
    chosenVerbInabilityParticle: '',
    chosenVerbNegativeParticle: '',
    chosenVerbTAM2Particle: '',
    chosenVerbTAM3Particle: '',
    chosenVerbTAM4Particle: '',
    chosenVerbPersonParticle: '',
    chosenVerbModalityParticle: '',
    chosenVerbSubordinatingSuffixParticle: '',
    chosenVerbPossessionMarkerParticle: '',
    chosenVerbCaseMarkerParticle: '',
    
  // USERS
    userEmail: '',
    authState: false,
    bookmarkedLessonNumber: null,
    usersBookmarkID: '',
}

const reducer = (state = initialState, action) => {
    switch (action.type) {

      // LESSONS
      case SET_LESSON_NUMBER: return {...state, chosenLessonNumber: action.chosenLessonNumber};
      case SET_LESSON_NAME: return {...state, chosenLessonName: action.chosenLessonName};
      case SET_LESSON_COMMENTARY_ID: return {...state, chosenLessonCommentaryID: action.chosenLessonCommentaryID};
      case SET_LESSON_LAYOUT: return {...state, chosenLessonLayout: action.chosenLessonLayout};

      // VERB ENGINE
      case SET_CHOSEN_VERB_TURKISH:
        return {...state, chosenVerbTurkish: action.chosenVerbTurkish};
      case SET_CHOSEN_VERB_STEM:
        return {...state, chosenVerbStem: action.chosenVerbStem};
      case SET_CHOSEN_VERB_ENGLISH:
        return {...state, chosenVerbEnglish: action.chosenVerbEnglish};
      case SET_CHOSEN_VERB_ENGLISH_GERUND:
        return {...state, chosenVerbEnglishGerund: action.chosenVerbEnglishGerund};
      case SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_SINGULAR_PRESENT:
        return {...state, chosenVerbEnglishThirdPersonSingularPresent: action.chosenVerbEnglishThirdPersonSingularPresent};
      case SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PRESENT:
        return {...state, chosenVerbEnglishThirdPersonPluralPresent: action.chosenVerbEnglishThirdPersonPluralPresent};
      case SET_CHOSEN_VERB_ENGLISH_PAST:
        return {...state, chosenVerbEnglishPast: action.chosenVerbEnglishPast};
      case SET_CHOSEN_VERB_ENGLISH_THIRD_PERSON_PLURAL_PAST:
        return {...state, chosenVerbEnglishThirdPersonPluralPast: action.chosenVerbEnglishThirdPersonPluralPast};
      case SET_CHOSEN_VERB_ENGLISH_PAST_PARTICIPLE:
        return {...state, chosenVerbEnglishPastParticiple: action.chosenVerbEnglishPastParticiple};
      case SET_CHOSEN_VERB_PASSIVE_AVAILABLE:
        return {...state, chosenVerbTurkishPassiveAvailable: action.chosenVerbTurkishPassiveAvailable};
      case SET_CHOSEN_VERB_VOICE:
        return {...state, chosenVerbVoice: action.chosenVerbVoice};
      case SET_CHOSEN_VERB_EXTRA_CAUSATIVE:
        return {...state, chosenVerbExtraCausative: action.chosenVerbExtraCausative};
      case SET_CHOSEN_VERB_INABILITY:
        return {...state, chosenVerbInability: action.chosenVerbInability};
      case SET_CHOSEN_VERB_NEGATIVE:
        return {...state, chosenVerbNegative: action.chosenVerbNegative};
      case SET_CHOSEN_VERB_FINITENESS:
        return {...state, chosenVerbFiniteness: action.chosenVerbFiniteness};
      case SET_CHOSEN_VERB_TAM2:
        return {...state, chosenVerbTAM2: action.chosenVerbTAM2};
      case SET_CHOSEN_VERB_TAM3:
        return {...state, chosenVerbTAM3: action.chosenVerbTAM3};
      case SET_CHOSEN_VERB_TAM4:
        return {...state, chosenVerbTAM4: action.chosenVerbTAM4};
      case SET_CHOSEN_VERB_PERSON:
        return {...state, chosenVerbPerson: action.chosenVerbPerson};
      case SET_CHOSEN_VERB_MODALITY:
        return {...state, chosenVerbModality: action.chosenVerbModality};
      case SET_CHOSEN_VERB_SUBORDINATING_SUFFIX:
        return {...state, chosenVerbSubordinatingSuffix: action.chosenVerbSubordinatingSuffix};
      case SET_CHOSEN_VERB_POSSESSION_MARKER:
        return {...state, chosenVerbPossessionMarker: action.chosenVerbPossessionMarker};
      case SET_CHOSEN_VERB_CASE_MARKER:
        return {...state, chosenVerbCaseMarker: action.chosenVerbCaseMarker};
      case SET_CHOSEN_VERB_VOICE_PARTICLE:
        return {...state, chosenVerbVoiceParticle: action.chosenVerbVoiceParticle};
      case SET_CHOSEN_VERB_EXTRA_CAUSATIVE_PARTICLE:
        return {...state, chosenVerbExtraCausativeParticle: action.chosenVerbExtraCausativeParticle};
      case SET_CHOSEN_VERB_INABILITY_PARTICLE:
        return {...state, chosenVerbInabilityParticle: action.chosenVerbInabilityParticle};
      case SET_CHOSEN_VERB_NEGATIVE_PARTICLE:
        return {...state, chosenVerbNegativeParticle: action.chosenVerbNegativeParticle};
      case SET_CHOSEN_VERB_TAM2_PARTICLE:
          return {...state, chosenVerbTAM2Particle: action.chosenVerbTAM2Particle};
      case SET_CHOSEN_VERB_TAM3_PARTICLE:
          return {...state, chosenVerbTAM3Particle: action.chosenVerbTAM3Particle};
      case SET_CHOSEN_VERB_TAM4_PARTICLE:
          return {...state, chosenVerbTAM4Particle: action.chosenVerbTAM4Particle};
      case SET_CHOSEN_VERB_PERSON_PARTICLE:
          return {...state, chosenVerbPersonParticle: action.chosenVerbPersonParticle};
      case SET_CHOSEN_VERB_MODALITY_PARTICLE:
          return {...state, chosenVerbModalityParticle: action.chosenVerbModalityParticle};
      case SET_CHOSEN_VERB_SUBORDINATING_SUFFIX_PARTICLE:
          return {...state, chosenVerbSubordinatingSuffixParticle: action.chosenVerbSubordinatingSuffixParticle};
      case SET_CHOSEN_VERB_POSSESSION_MARKER_PARTICLE:
          return {...state, chosenVerbPossessionMarkerParticle: action.chosenVerbPossessionMarkerParticle};
      case SET_CHOSEN_VERB_CASE_MARKER_PARTICLE:
          return {...state, chosenVerbCaseMarkerParticle: action.chosenVerbCaseMarkerParticle};
        
        // USERS
      case SET_USER_EMAIL: return {...state, userEmail: action.userEmail};
      case SET_AUTH_STATE: return {...state, authState: action.authState};
      case COMMIT_BOOKMARK_TO_CENTRAL_STORE: return {...state, bookmarkedLessonNumber: action.bookmarkedLessonNumber};
      case SET_USERS_BOOKMARK_ID: return {...state, usersBookmarkID: action.usersBookmarkID};

      default: return state;
    }
  };

const tepkiReducers = combineReducers({
  reducer
});
  
export default tepkiReducers;